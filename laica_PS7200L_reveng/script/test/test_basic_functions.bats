#!/usr/bin/env bats

setup() {
    . ./basic_functions.sh
}

bluetooth_start() {
    rf_out=$( rfkill list )

    while read line
    do
        #find bluetooth adapters indices in rfkill list
        if [[ "$line" =~ [B|b]luetooth ]]; then
            index[${#index[@]}]=$( echo $line | cut -d ":" -f 1 )
        fi
    done <<< "$rf_out"

    #if no line contains "bluetooth" the test fails
    [ "${#index[@]}" -gt 0 ]

    #activate bluetooth on each adapter
    for adapter in "${index[@]}"; do
        rfkill unblock $adapter
    done

    sleep 2
}

bluetooth_stop() {
    rf_out=$( rfkill list )

    while read line
    do
        #find bluetooth adapters indices in rfkill list
        if [[ "$line" =~ [B|b]luetooth ]]; then
            index[${#index[@]}]=$( echo $line | cut -d ":" -f 1 )
        fi
    done <<< "$rf_out"

    #if no line contains "bluetooth" the test fails
    [ "${#index[@]}" -gt 0 ]

    #deactivate bluetooth on each adapter
    for adapter in "${index[@]}"; do
        rfkill block $adapter
    done
}

@test "MAC address format conversion" {
    run format_address 9c:b6:0e:61:c2:3d
    [ "$output" = "3d c2 61 0e b6 9c" ]
    [ "$status" -eq 0 ]

    run format_address 9C:B6:0E:61:C2:3D
    [ "$output" = "3D C2 61 0E B6 9C" ]
    [ "$status" -eq 0 ]

    #invalid MAC address syntax causes an error
    run format_address 9c:b6:d0
    [ "$status" -ne 0 ]

    #invalid MAC address syntax causes an error
    run format_address 9C:B6:0E:GG:C2:3D
    [ "$status" -ne 0 ]

    #lack of parameters causes an error
    run format_address
    [ "$output" = "Usage: format_address <MAC_address>" ]
    [ "$status" -ne 0 ]
}

@test "address scan" {
    bluetooth_start

    #no address found case
    run scan_address devicename test_file.txt 5
    [ "$output" = "-1" ]
    [ "$status" -eq 0 ]
    [ -e test_file.txt ]
    rm test_file.txt

    #lack of parameters causes an error
    run scan_address devicename
    [ "$output" = "Usage: scan_address <device_name> <file_name> [timeout]" ]
    [ "$status" -ne 0 ]

    bluetooth_stop
}

@test "find weight" {
    run find_weight FF:FF:FF:FF:FF:D0 hcidump_output
    [ "$output" = "76.00" ]
    [ "$status" -eq 0 ]

    #-1 (but exit status 0) if MAC address not present in hcidump_file
    run find_weight 9C:B6:0E:FF:C2:3D hcidump_output
    [ "$output" = "-1" ]
    [ "$status" -eq 0 ]

    #missing file causes and error
    run find_weight 9C:B6:0E:FF:C2:3D missing_file
    [ "$output" = "find_weight() error: the hcidump required file does not exists" ]
    [ "$status" -ne 0 ]

    #lack of parameters causes an error
    run find_weight hcidump_output
    [ "$output" = "Usage: scan_address <MAC_address> <hcidump_file>" ]
    [ "$status" -ne 0 ]

    #invalid MAC address syntax causes an error
    run find_weight 9c:b6:d0 hcidump_output
    [ "$status" -ne 0 ]

    #invalid MAC address syntax causes an error
    run find_weight 9C:B6:0E:GG:C2:3D hcidump_output
    [ "$status" -ne 0 ]
}

@test "bluetooth status checker" {
    bluetooth_start
    run check_bt_status
    [ "$status" -eq 0 ]

    bluetooth_stop
    run check_bt_status
    [ "$status" -ne 0 ]
}
