package org.androidannotations.api.sharedpreferences;

import android.content.SharedPreferences;

public final class LongPrefField extends AbstractPrefField {
    private final long defaultValue;

    LongPrefField(SharedPreferences sharedPreferences, String key, long defaultValue) {
        super(sharedPreferences, key);
        this.defaultValue = defaultValue;
    }

    public long get() {
        return getOr(this.defaultValue);
    }

    public long getOr(long defaultValue) {
        long j;
        try {
            j = this.sharedPreferences.getLong(this.key, defaultValue);
        } catch (ClassCastException e) {
            try {
                j = Long.parseLong(this.sharedPreferences.getString(this.key, "" + defaultValue));
            } catch (Exception e2) {
                throw e;
            }
        }
        return j;
    }

    public void put(long value) {
        apply(edit().putLong(this.key, value));
    }
}
