package org.androidannotations.api.sharedpreferences;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

public abstract class SharedPreferencesCompat {
    private static final Method sApplyMethod = findMethod(Editor.class, "apply", new Class[0]);
    private static final Method sGetStringSetMethod = findMethod(SharedPreferences.class, "getStringSet", String.class, Set.class);
    private static final Method sPutStringSetMethod = findMethod(Editor.class, "putStringSet", String.class, Set.class);

    private SharedPreferencesCompat() {
    }

    public static void apply(Editor editor) {
        try {
            invoke(sApplyMethod, editor, new Object[0]);
        } catch (NoSuchMethodException e) {
            editor.commit();
        }
    }

    public static Set<String> getStringSet(SharedPreferences preferences, String key, Set<String> defValues) {
        try {
            return (Set) invoke(sGetStringSetMethod, preferences, key, defValues);
        } catch (NoSuchMethodException e) {
            return SetXmlSerializer.deserialize(preferences.getString(key, null));
        }
    }

    public static void putStringSet(Editor editor, String key, Set<String> values) {
        try {
            invoke(sPutStringSetMethod, editor, key, values);
        } catch (NoSuchMethodException e) {
            editor.putString(key, SetXmlSerializer.serialize(values));
        }
    }

    private static Method findMethod(Class<?> clazz, String name, Class<?>... parameterTypes) {
        try {
            return clazz.getMethod(name, parameterTypes);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    public static <T> T invoke(Method method, Object obj, Object... args) throws NoSuchMethodException {
        if (method == null) {
            throw new NoSuchMethodException();
        }
        try {
            return method.invoke(obj, args);
        } catch (IllegalAccessException e) {
            throw new NoSuchMethodException(method.getName());
        } catch (IllegalArgumentException e2) {
            throw new NoSuchMethodException(method.getName());
        } catch (InvocationTargetException e3) {
            throw new NoSuchMethodException(method.getName());
        }
    }
}
