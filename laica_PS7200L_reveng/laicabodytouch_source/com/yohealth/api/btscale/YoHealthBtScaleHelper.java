package com.yohealth.api.btscale;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.MotionEventCompat;
import com.example.hellojni.HelloJni;

public class YoHealthBtScaleHelper {
    public static final int Female = 1;
    public static final int Male = 0;
    private static final long SCAN_PERIOD = 10000;
    protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static final int requestCode = 10005;
    int age;
    private String factory = "01,V1.1";
    int height;
    boolean isLockData = false;
    private boolean isOpenBle = false;
    private BluetoothAdapter mBluetoothAdapter;
    private Context mContext;
    Handler mHandler = new C01832();
    private LeScanCallback mLeScanCallback = new C01821();
    private boolean mScanning;
    OnYoWeightListener onYoWeightListener;
    int sex;
    HelloJni sohelper;

    class C01821 implements LeScanCallback {
        C01821() {
        }

        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            try {
                String deviceName = device.getName();
                if (deviceName != null && deviceName.equals("YoHealth")) {
                    String scanRecordStr = YoHealthBtScaleHelper.bytesToHex(scanRecord);
                    String str = scanRecordStr;
                    String val = str.substring(scanRecordStr.indexOf("02"), scanRecordStr.indexOf("02") + 28);
                    int deviceMode = ObjectHelper.Convert2Int(ObjectHelper.GetBluetoothValIndex(val, 11));
                    float weight = (float) Integer.parseInt(ObjectHelper.GetBluetoothValIndex(val, 4) + ObjectHelper.GetBluetoothValIndex(val, 5), 16);
                    boolean isHighPrecision = false;
                    if (deviceMode % 10 == 1) {
                        weight /= 10.0f;
                    } else if (deviceMode % 10 == 2) {
                        weight /= 100.0f;
                        isHighPrecision = true;
                    }
                    if (deviceMode > 10 && deviceMode < 20) {
                        deviceMode = 0;
                    } else if (deviceMode > 20 && deviceMode < 30) {
                        deviceMode = 1;
                    }
                    String[] valArray = YoHealthBtScaleHelper.this.sohelper.getHealth(YoHealthBtScaleHelper.this.sex, YoHealthBtScaleHelper.this.age, YoHealthBtScaleHelper.this.height, (double) weight, Integer.parseInt(ObjectHelper.GetBluetoothValIndex(val, 6) + ObjectHelper.GetBluetoothValIndex(val, 7), 16)).split(",");
                    float bmi = ObjectHelper.Convert2MathCount(1, Float.valueOf(ObjectHelper.Convert2Float(valArray[0])));
                    int devicestate = Integer.parseInt(ObjectHelper.GetBluetoothValIndex(val, 8), 16);
                    int state = 0;
                    if ((((int) Math.pow(2.0d, 0.0d)) & devicestate) > 0) {
                        state = 1;
                    }
                    Object obj;
                    Message message;
                    if ((((int) Math.pow(2.0d, 1.0d)) & devicestate) <= 0) {
                        YoHealthBtScaleHelper.this.isLockData = false;
                        if (YoHealthBtScaleHelper.this.onYoWeightListener != null) {
                            obj = new Object[]{Boolean.valueOf(isHighPrecision), Integer.valueOf(deviceMode), Float.valueOf(weight), Float.valueOf(bmi)};
                            message = new Message();
                            message.what = 1;
                            message.obj = obj;
                            YoHealthBtScaleHelper.this.mHandler.sendMessage(message);
                        }
                    } else if (!YoHealthBtScaleHelper.this.isLockData) {
                        YoHealthBtScaleHelper.this.isLockData = true;
                        float zhifang = 0.0f;
                        float jirou = 0.0f;
                        float shuifen = 0.0f;
                        float guluo = 0.0f;
                        float insidefat = 0.0f;
                        int insideage = 0;
                        if ((devicestate & 15) != 2) {
                            zhifang = ObjectHelper.Convert2MathCount(1, Float.valueOf(ObjectHelper.Convert2Float(valArray[1]) * 100.0f));
                            shuifen = ObjectHelper.Convert2MathCount(1, Float.valueOf(ObjectHelper.Convert2Float(valArray[2]) * 100.0f));
                            jirou = ObjectHelper.Convert2MathCount(1, Float.valueOf(ObjectHelper.Convert2Float(valArray[3]) * 100.0f));
                            guluo = ObjectHelper.Convert2Float(valArray[4]) / 30.0f;
                            insidefat = ObjectHelper.Convert2MathCount(1, Float.valueOf(ObjectHelper.Convert2Float(valArray[5]) * 100.0f));
                            insideage = ObjectHelper.Convert2Int(valArray[6]);
                            if (zhifang < 1.0f) {
                                zhifang = 1.0f;
                            }
                            if (shuifen > 90.0f) {
                                shuifen = 90.0f;
                            }
                            if (jirou > 90.0f) {
                                jirou = 90.0f;
                            }
                        } else if (deviceMode == 1) {
                            state = 3;
                        }
                        int basalmetabolism = ObjectHelper.Convert2Int(valArray[7]);
                        if (YoHealthBtScaleHelper.this.onYoWeightListener != null) {
                            obj = new Object[]{Boolean.valueOf(isHighPrecision), Integer.valueOf(deviceMode), Integer.valueOf(state), Float.valueOf(weight), Float.valueOf(bmi), Float.valueOf(zhifang), Float.valueOf(jirou), Float.valueOf(shuifen), Float.valueOf(guluo), Float.valueOf(insidefat), Integer.valueOf(insideage), Integer.valueOf(basalmetabolism)};
                            message = new Message();
                            message.what = 2;
                            message.obj = obj;
                            YoHealthBtScaleHelper.this.mHandler.sendMessage(message);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C01832 extends Handler {
        C01832() {
        }

        public void handleMessage(Message msg) {
            Object[] objs;
            if (msg.what == 1) {
                objs = msg.obj;
                YoHealthBtScaleHelper.this.onYoWeightListener.realtimeData(((Boolean) objs[0]).booleanValue(), ObjectHelper.Convert2Int(objs[1]), ObjectHelper.Convert2Float(objs[2]), ObjectHelper.Convert2Float(objs[3]));
            } else if (msg.what == 2) {
                objs = (Object[]) msg.obj;
                YoHealthBtScaleHelper.this.onYoWeightListener.stableData(((Boolean) objs[0]).booleanValue(), ObjectHelper.Convert2Int(objs[1]), ObjectHelper.Convert2Int(objs[2]), ObjectHelper.Convert2Float(objs[3]), ObjectHelper.Convert2Float(objs[4]), ObjectHelper.Convert2Float(objs[5]), ObjectHelper.Convert2Float(objs[6]), ObjectHelper.Convert2Float(objs[7]), ObjectHelper.Convert2Float(objs[8]), ObjectHelper.Convert2Float(objs[9]), ObjectHelper.Convert2Int(objs[10]), ObjectHelper.Convert2Int(objs[11]));
                YoHealthBtScaleHelper.this.onYoWeightListener.getFactory(YoHealthBtScaleHelper.this.getFactory());
            }
            super.handleMessage(msg);
        }
    }

    class C01843 extends BroadcastReceiver {
        C01843() {
        }

        public void onReceive(Context context, Intent intent) {
            switch (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -1)) {
                case 10:
                    YoHealthBtScaleHelper.this.isOpenBle = false;
                    return;
                case 12:
                    YoHealthBtScaleHelper.this.isOpenBle = true;
                    return;
                default:
                    return;
            }
        }
    }

    public interface OnYoWeightListener {
        public static final int FatDevice = 1;
        public static final int NoFatDevice = 0;
        public static final int State_LowVoltage = 2;
        public static final int State_Normal = 0;
        public static final int State_OverWeight = 1;
        public static final int State_WearOutShoe = 3;

        void error(String str);

        void getFactory(String str);

        void realtimeData(boolean z, int i, float f, float f2);

        void stableData(boolean z, int i, int i2, float f, float f2, float f3, float f4, float f5, float f6, float f7, int i3, int i4);
    }

    private String getFactory() {
        return this.factory;
    }

    static {
        System.loadLibrary("yohealth");
    }

    public YoHealthBtScaleHelper(Context c, int height, int age, int sex) {
        this.mContext = c;
        this.height = height;
        this.age = age;
        this.sex = sex;
    }

    public void setMemberInfo(int height, int age, int sex) {
        this.height = height;
        this.age = age;
        this.sex = sex;
    }

    public void setOnYoWeightListener(OnYoWeightListener onYoWeightListener) {
        this.onYoWeightListener = onYoWeightListener;
    }

    public void init() {
        if (VERSION.SDK_INT >= 18) {
            try {
                this.sohelper = new HelloJni();
                this.mBluetoothAdapter = ((BluetoothManager) this.mContext.getSystemService("bluetooth")).getAdapter();
                if (this.mBluetoothAdapter == null || !this.mBluetoothAdapter.isEnabled()) {
                    ((Activity) this.mContext).startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), requestCode);
                } else {
                    this.isOpenBle = true;
                }
                this.mContext.registerReceiver(new C01843(), new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
                return;
            } catch (Exception e) {
                sendError("Initial exception");
                return;
            }
        }
        sendError("The Android system is too low，it can't support BLE4.0");
    }

    private void scanLeDevice(boolean enable) {
        if (enable) {
            this.mScanning = true;
            if (this.mBluetoothAdapter != null) {
                this.mBluetoothAdapter.startLeScan(this.mLeScanCallback);
                return;
            }
            return;
        }
        this.mScanning = false;
        if (this.mBluetoothAdapter != null) {
            this.mBluetoothAdapter.stopLeScan(this.mLeScanCallback);
        }
    }

    public boolean start() {
        if (!this.isOpenBle) {
            return false;
        }
        scanLeDevice(true);
        return true;
    }

    public void stop() {
        scanLeDevice(false);
    }

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[(bytes.length * 2)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & MotionEventCompat.ACTION_MASK;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[(j * 2) + 1] = hexArray[v & 15];
        }
        return new String(hexChars);
    }

    private void sendError(String msg) {
        if (this.onYoWeightListener != null) {
            this.onYoWeightListener.error(msg);
        }
    }
}
