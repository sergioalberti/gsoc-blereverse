package com.yohealth.api.btscale;

import java.math.BigDecimal;

public class ObjectHelper {
    public static int Convert2Int(Object obj) {
        try {
            return Integer.valueOf(obj.toString().trim()).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static float Convert2MathCount(int count, Object obj) {
        float f = 0.0f;
        if (obj != null) {
            try {
                f = new BigDecimal((double) Float.valueOf(obj.toString()).floatValue()).setScale(count, 4).floatValue();
            } catch (Exception e) {
            }
        }
        return f;
    }

    public static float Convert2Float(Object val) {
        try {
            return Float.valueOf(val.toString()).floatValue();
        } catch (Exception e) {
            return 0.0f;
        }
    }

    public static String GetBluetoothValIndex(String str, int i) {
        try {
            return str.substring(i * 2, (i + 1) * 2);
        } catch (Exception e) {
            return "";
        }
    }
}
