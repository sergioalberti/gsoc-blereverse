package com.whb.loease.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.builder.ActivityIntentBuilder;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class TargetViewActivity_ extends TargetViewActivity implements HasViews, OnViewChangedListener {
    public static final String CLIENT_ID_EXTRA = "EXTRA_KEY_ID_STRING";
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01781 implements OnClickListener {
        C01781() {
        }

        public void onClick(View view) {
            TargetViewActivity_.this.goBack();
        }
    }

    class C01792 implements OnClickListener {
        C01792() {
        }

        public void onClick(View view) {
            TargetViewActivity_.this.showSetTargetView();
        }
    }

    class C01803 implements OnClickListener {
        C01803() {
        }

        public void onClick(View view) {
            TargetViewActivity_.this.saveTarget();
        }
    }

    public static class IntentBuilder_ extends ActivityIntentBuilder<IntentBuilder_> {
        private Fragment fragmentSupport_;
        private android.app.Fragment fragment_;

        public IntentBuilder_(Context context) {
            super(context, TargetViewActivity_.class);
        }

        public IntentBuilder_(android.app.Fragment fragment) {
            super(fragment.getActivity(), TargetViewActivity_.class);
            this.fragment_ = fragment;
        }

        public IntentBuilder_(Fragment fragment) {
            super(fragment.getActivity(), TargetViewActivity_.class);
            this.fragmentSupport_ = fragment;
        }

        public void startForResult(int requestCode) {
            if (this.fragmentSupport_ != null) {
                this.fragmentSupport_.startActivityForResult(this.intent, requestCode);
            } else if (this.fragment_ != null) {
                this.fragment_.startActivityForResult(this.intent, requestCode);
            } else {
                super.startForResult(requestCode);
            }
        }

        public IntentBuilder_ ClientId(String ClientId) {
            return (IntentBuilder_) super.extra("EXTRA_KEY_ID_STRING", ClientId);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        requestWindowFeature(1);
        injectExtras_();
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view) {
        super.setContentView(view);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static IntentBuilder_ intent(Context context) {
        return new IntentBuilder_(context);
    }

    public static IntentBuilder_ intent(android.app.Fragment fragment) {
        return new IntentBuilder_(fragment);
    }

    public static IntentBuilder_ intent(Fragment supportFragment) {
        return new IntentBuilder_(supportFragment);
    }

    public void onViewChanged(HasViews hasViews) {
        this.btnBack = (Button) hasViews.findViewById(C0181R.id.btnBack);
        this.tvTitle = (TextView) hasViews.findViewById(C0181R.id.tvTitle);
        this.tvConfirm = (TextView) hasViews.findViewById(C0181R.id.tvConfirm);
        this.rlRoot = (RelativeLayout) hasViews.findViewById(C0181R.id.rlRoot);
        this.tvTotargetVal = (TextView) hasViews.findViewById(C0181R.id.tvTotargetVal);
        this.tvStillHave = (TextView) hasViews.findViewById(C0181R.id.tvStillHave);
        this.llTargetView = (LinearLayout) hasViews.findViewById(C0181R.id.llTargetView);
        this.rlTargetCircle = (RelativeLayout) hasViews.findViewById(C0181R.id.rlTargetCircle);
        this.tvLeftDays = (TextView) hasViews.findViewById(C0181R.id.tvLeftDays);
        this.tvWtUnit = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit);
        this.tvTargetWtVal = (TextView) hasViews.findViewById(C0181R.id.tvTargetWtVal);
        this.tvCurrentWtVal = (TextView) hasViews.findViewById(C0181R.id.tvCurrentWtVal);
        this.rlTop = (RelativeLayout) hasViews.findViewById(C0181R.id.rlTop);
        this.llViews = (LinearLayout) hasViews.findViewById(C0181R.id.llViews);
        if (this.btnBack != null) {
            this.btnBack.setOnClickListener(new C01781());
        }
        View view = hasViews.findViewById(C0181R.id.btnSetTarget);
        if (view != null) {
            view.setOnClickListener(new C01792());
        }
        if (this.tvConfirm != null) {
            this.tvConfirm.setOnClickListener(new C01803());
        }
        InitBase();
        init();
    }

    private void injectExtras_() {
        Bundle extras_ = getIntent().getExtras();
        if (extras_ != null && extras_.containsKey("EXTRA_KEY_ID_STRING")) {
            this.ClientId = extras_.getString("EXTRA_KEY_ID_STRING");
        }
    }

    public void setIntent(Intent newIntent) {
        super.setIntent(newIntent);
        injectExtras_();
    }
}
