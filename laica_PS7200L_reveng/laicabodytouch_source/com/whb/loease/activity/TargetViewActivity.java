package com.whb.loease.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.vc.cloudbalance.common.BaseActivity;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.sqlite.BalanceDataDAL;
import com.vc.cloudbalance.sqlite.MemberDAL;
import com.vc.cloudbalance.widget.ViewSetTarget;
import com.vc.cloudbalance.widget.ViewSetTarget.OnFinishListener;
import com.vc.cloudbalance.widget.ViewSetTarget_;
import com.vc.cloudbalance.widget.balanceCircleView;
import com.vc.util.ObjectHelper;
import com.whb.loease.bodytouch.C0181R;
import java.text.ParseException;
import java.util.Date;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@EActivity
public class TargetViewActivity extends BaseActivity {
    @Extra("EXTRA_KEY_ID_STRING")
    String ClientId;
    private boolean isHighPrecision = false;
    private boolean isbigCircleLoad = false;
    @ViewById
    LinearLayout llTargetView;
    @ViewById
    LinearLayout llViews;
    MemberMDL member;
    private boolean onShowPopwidow = false;
    @ViewById
    RelativeLayout rlRoot;
    @ViewById
    RelativeLayout rlTargetCircle;
    @ViewById
    RelativeLayout rlTop;
    ViewSetTarget setTargetView;
    balanceCircleView targetCircleView;
    @ViewById
    TextView tvConfirm;
    @ViewById
    TextView tvCurrentWtVal;
    @ViewById
    TextView tvLeftDays;
    @ViewById
    TextView tvStillHave;
    @ViewById
    TextView tvTargetWtVal;
    @ViewById
    TextView tvTotargetVal;
    @ViewById
    TextView tvWtUnit;
    private int wtCircleWidth = 700;

    class C02801 implements OnFinishListener {
        C02801() {
        }

        public void onFinish() {
            TargetViewActivity.this.setTargetView.setVisibility(8);
            TargetViewActivity.this.llTargetView.setVisibility(0);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initThemeView();
    }

    private void initThemeView() {
        switch (Common.getThemeType()) {
            case 1:
                setContentView(C0181R.layout.activity_target_view);
                return;
            case 2:
                setContentView(C0181R.layout.activity_target_view_greeentheme);
                return;
            case 4:
                setContentView(C0181R.layout.activity_target_view_greytheme);
                return;
            default:
                setContentView(C0181R.layout.activity_target_view_bluetheme);
                return;
        }
    }

    protected void onResume() {
        super.onResume();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0 || this.setTargetView == null || this.setTargetView.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.setTargetView.setVisibility(8);
        this.llTargetView.setVisibility(0);
        this.tvConfirm.setVisibility(8);
        this.tvTitle.setText(getString(C0181R.string.set_target));
        return false;
    }

    public void goBack() {
        if (this.setTargetView == null) {
            super.goBack();
        } else if (this.setTargetView.getVisibility() == 0) {
            this.setTargetView.setVisibility(8);
            this.llTargetView.setVisibility(0);
            this.tvConfirm.setVisibility(8);
            this.tvTitle.setText(getString(C0181R.string.target));
        } else {
            super.goBack();
        }
    }

    @Click({2131493015})
    void showSetTargetView() {
        this.tvTitle.setText(getString(C0181R.string.set_target));
        if (this.setTargetView == null) {
            this.setTargetView = ViewSetTarget_.build(this.mContext, this.member);
            this.setTargetView.setOnFinishListener(new C02801());
            this.llViews.addView(this.setTargetView);
        }
        this.onShowPopwidow = true;
        this.llTargetView.setVisibility(8);
        this.setTargetView.setVisibility(0);
    }

    @Click({2131493005})
    void saveTarget() {
    }

    @AfterViews
    void init() {
        if (new AppConfigDAL(this.mContext).select(Constants.DEVICETYPE_STRING).equals("2")) {
            this.isHighPrecision = true;
        }
        this.member = new MemberDAL(this).SelectByClientId(this.ClientId);
        this.tvWtUnit.setText(WeightUnitHelper.getWeightUnitString());
        setData();
    }

    private void drawBigCircle(final float currentVal, final float targetVal) {
        if (!this.isbigCircleLoad) {
            this.rlTargetCircle.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    if (!TargetViewActivity.this.isbigCircleLoad) {
                        TargetViewActivity.this.isbigCircleLoad = true;
                        int wtCircleWidth = TargetViewActivity.this.rlTargetCircle.getWidth();
                        TargetViewActivity.this.targetCircleView = new balanceCircleView(TargetViewActivity.this.mContext, currentVal, targetVal, wtCircleWidth);
                        int strokeWidth = TargetViewActivity.this.getResources().getDimensionPixelSize(C0181R.dimen.banlancecircle_thickstrokewidth);
                        TargetViewActivity.this.rlTargetCircle.addView(TargetViewActivity.this.targetCircleView, new LayoutParams(wtCircleWidth + strokeWidth, wtCircleWidth + strokeWidth));
                    }
                }
            });
        } else if (this.targetCircleView != null) {
            this.targetCircleView.invalidate(currentVal, targetVal);
        }
    }

    public void setData() {
        float currentVal = 0.0f;
        float targetVal = 0.0f;
        this.tvTotargetVal.setTextSize(1, 45.0f);
        BalanceDataMDL lastData = new BalanceDataDAL(this).SelectLastData(this.member.getMemberid(), this.member.getClientid());
        if (lastData != null) {
            currentVal = ObjectHelper.Convert2Float(lastData.getWeight());
            this.tvCurrentWtVal.setText(WeightUnitHelper.getConvertedWtVal(this.isHighPrecision, 2, Float.valueOf(currentVal)));
        } else {
            this.tvCurrentWtVal.setText("——");
        }
        if (this.member.getTargetweight() != null) {
            targetVal = ObjectHelper.Convert2Float(this.member.getTargetweight());
            this.tvTargetWtVal.setText(WeightUnitHelper.getConvertedWtVal(true, 2, Float.valueOf(targetVal)));
            if (lastData == null) {
                this.tvTotargetVal.setTextSize(1, 25.0f);
                this.tvTotargetVal.setText(getString(C0181R.string.noweighing));
            } else if (targetVal - currentVal > 0.0f) {
                this.tvTotargetVal.setText("+" + WeightUnitHelper.getConvertedWtVal(true, 2, Float.valueOf(targetVal - currentVal)));
            } else {
                this.tvTotargetVal.setText(WeightUnitHelper.getConvertedWtVal(true, 2, Float.valueOf(targetVal - currentVal)));
            }
            this.tvStillHave.setText(getString(C0181R.string.totarget_remain));
            int startToFinishDays = 0;
            try {
                startToFinishDays = ObjectHelper.daysBetween(ObjectHelper.Convert2String(new Date(), "yyyy-MM-dd"), this.member.getTargetFinishTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (startToFinishDays < 0) {
                startToFinishDays = 0;
            }
            this.tvLeftDays.setText(new StringBuilder(String.valueOf(startToFinishDays)).append(getString(C0181R.string.day)).toString());
        } else {
            this.tvTotargetVal.setText(getString(C0181R.string.notarget));
            this.tvTargetWtVal.setText("——");
        }
        drawBigCircle(currentVal, targetVal);
    }
}
