package com.whb.loease.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import com.whb.loease.bodytouch.C0181R;
import de.hdodenhof.circleimageview.CircleImageView;
import org.androidannotations.api.builder.ActivityIntentBuilder;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class MainActivity_ extends MainActivity implements HasViews, OnViewChangedListener {
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01511 implements OnClickListener {
        C01511() {
        }

        public void onClick(View view) {
            MainActivity_.this.goBack();
        }
    }

    class C01522 implements OnClickListener {
        C01522() {
        }

        public void onClick(View view) {
            MainActivity_.this.Experience();
        }
    }

    class C01533 implements OnClickListener {
        C01533() {
        }

        public void onClick(View view) {
            MainActivity_.this.showTargetViewActivity();
        }
    }

    class C01544 implements OnClickListener {
        C01544() {
        }

        public void onClick(View view) {
            MainActivity_.this.showMemberList();
        }
    }

    class C01555 implements OnClickListener {
        C01555() {
        }

        public void onClick(View view) {
            MainActivity_.this.showDataViewActivity();
        }
    }

    class C01566 implements OnClickListener {
        C01566() {
        }

        public void onClick(View view) {
            MainActivity_.this.showChildBalanceActivity();
        }
    }

    class C01577 implements OnClickListener {
        C01577() {
        }

        public void onClick(View view) {
            MainActivity_.this.showSettingActivity();
        }
    }

    public static class IntentBuilder_ extends ActivityIntentBuilder<IntentBuilder_> {
        private Fragment fragmentSupport_;
        private android.app.Fragment fragment_;

        public IntentBuilder_(Context context) {
            super(context, MainActivity_.class);
        }

        public IntentBuilder_(android.app.Fragment fragment) {
            super(fragment.getActivity(), MainActivity_.class);
            this.fragment_ = fragment;
        }

        public IntentBuilder_(Fragment fragment) {
            super(fragment.getActivity(), MainActivity_.class);
            this.fragmentSupport_ = fragment;
        }

        public void startForResult(int requestCode) {
            if (this.fragmentSupport_ != null) {
                this.fragmentSupport_.startActivityForResult(this.intent, requestCode);
            } else if (this.fragment_ != null) {
                this.fragment_.startActivityForResult(this.intent, requestCode);
            } else {
                super.startForResult(requestCode);
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        requestWindowFeature(1);
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view) {
        super.setContentView(view);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static IntentBuilder_ intent(Context context) {
        return new IntentBuilder_(context);
    }

    public static IntentBuilder_ intent(android.app.Fragment fragment) {
        return new IntentBuilder_(fragment);
    }

    public static IntentBuilder_ intent(Fragment supportFragment) {
        return new IntentBuilder_(supportFragment);
    }

    public void onViewChanged(HasViews hasViews) {
        this.tvTitle = (TextView) hasViews.findViewById(C0181R.id.tvTitle);
        this.btnBack = (Button) hasViews.findViewById(C0181R.id.btnBack);
        this.tvWtUnit1 = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit1);
        this.tbDetailData = (TableLayout) hasViews.findViewById(C0181R.id.tbDetailData);
        this.tvBMIVal = (TextView) hasViews.findViewById(C0181R.id.tvBMIVal);
        this.tvFatVal = (TextView) hasViews.findViewById(C0181R.id.tvFatVal);
        this.circleImageView = (CircleImageView) hasViews.findViewById(C0181R.id.circleImageView);
        this.tvTargetWtVal = (TextView) hasViews.findViewById(C0181R.id.tvTargetWtVal);
        this.tvBMIVal2 = (TextView) hasViews.findViewById(C0181R.id.tvBMIVal2);
        this.rlWtCircle = (RelativeLayout) hasViews.findViewById(C0181R.id.rlWtCircle);
        this.tvMsg = (TextView) hasViews.findViewById(C0181R.id.tvMsg);
        this.tvMuscleVal = (TextView) hasViews.findViewById(C0181R.id.tvMuscleVal);
        this.tvWeightVal = (TextView) hasViews.findViewById(C0181R.id.tvWeightVal);
        this.llTop = (LinearLayout) hasViews.findViewById(C0181R.id.llTop);
        this.tvBoneVal = (TextView) hasViews.findViewById(C0181R.id.tvBoneVal);
        this.tvCalorieVal = (TextView) hasViews.findViewById(C0181R.id.tvCalorieVal);
        this.tvWaterVal = (TextView) hasViews.findViewById(C0181R.id.tvWaterVal);
        this.tvWtUnit2 = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit2);
        this.tvFatTip = (TextView) hasViews.findViewById(C0181R.id.tvFatTip);
        this.imgHead = (ImageView) hasViews.findViewById(C0181R.id.imgHead);
        this.llMemberName = (LinearLayout) hasViews.findViewById(C0181R.id.llMemberName);
        if (this.btnBack != null) {
            this.btnBack.setOnClickListener(new C01511());
        }
        View view = hasViews.findViewById(C0181R.id.llMore);
        if (view != null) {
            view.setOnClickListener(new C01522());
        }
        view = hasViews.findViewById(C0181R.id.llTarget);
        if (view != null) {
            view.setOnClickListener(new C01533());
        }
        if (this.llMemberName != null) {
            this.llMemberName.setOnClickListener(new C01544());
        }
        view = hasViews.findViewById(C0181R.id.llData);
        if (view != null) {
            view.setOnClickListener(new C01555());
        }
        view = hasViews.findViewById(C0181R.id.llBaby);
        if (view != null) {
            view.setOnClickListener(new C01566());
        }
        view = hasViews.findViewById(C0181R.id.llMemberHead);
        if (view != null) {
            view.setOnClickListener(new C01577());
        }
        InitBase();
        init();
    }
}
