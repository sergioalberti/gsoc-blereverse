package com.vc.cloudbalance.webservice;

import android.content.Context;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.net.RequestParams;
import com.vc.util.FileHelper;
import org.json.JSONObject;

public class BalanceDataWS extends BaseWS {
    public BalanceDataWS(Context context) {
        super(context);
    }

    public JSONObject insertBalanceData(BalanceDataMDL mdl) {
        try {
            String url = GetMethodURL("updateMamberdata3");
            RequestParams params = getParams();
            params.put("userid", mdl.getUserid());
            params.put("memberid", mdl.getMemberid());
            params.put("weidate", mdl.getWeidateString());
            params.put("weight", mdl.getWeight());
            params.put("bmi", mdl.getBmi());
            params.put("fatpercent", mdl.getFatpercent());
            params.put("muscle", mdl.getMuscle());
            params.put("bone", mdl.getBone());
            params.put("water", mdl.getWater());
            params.put("innerfat", mdl.getInnerfat());
            params.put("basalmetabolism", mdl.getBasalmetabolism());
            params.put("icondata", FileHelper.BytetoStreamString(mdl.getClientImg()));
            params.put("haveimg", mdl.getHaveimg());
            params.put("height", mdl.getHeight());
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getRanking2(String userid, String memberid, String bmi, String sex, String height, String weight) {
        try {
            String url = GetMethodURL("getRanking2");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("memberid", (Object) memberid);
            params.put("bmi", (Object) bmi);
            params.put("sex", (Object) sex);
            params.put("height", (Object) height);
            params.put("weight", (Object) weight);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getRankingChildren(String userid, String memberid, String birthday, String sex, String height, String weight) {
        try {
            String url = GetMethodURL("getRankingChildren");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("memberid", (Object) memberid);
            params.put("birthday", (Object) birthday);
            params.put("sex", (Object) sex);
            params.put("height", (Object) height);
            params.put("weight", (Object) weight);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getMamberdataCount(String userid) {
        try {
            String url = GetMethodURL("getMamberdataCount");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getMamberdata(String userid, String memberid) {
        try {
            String url = GetMethodURL("getMamberdata");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("memberid", (Object) memberid);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }
}
