package com.vc.cloudbalance.webservice;

import android.content.Context;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.model.ModeTypeEnum;
import com.vc.net.RequestParams;
import com.vc.util.FileHelper;
import org.json.JSONObject;

public class MemberWS extends BaseWS {
    public MemberWS(Context context) {
        super(context);
    }

    public JSONObject insertMember(String userid, MemberMDL member) {
        try {
            String url = GetMethodURL("updateMember");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("memberid", (Object) "");
            params.put("membername", member.getMembername());
            params.put("birthday", member.getBirthday());
            params.put("height", member.getHeight());
            params.put("waist", member.getWaist());
            params.put("sex", member.getSex());
            params.put("targetweight", member.getTargetweight());
            params.put("modeltype", member.getModeltype());
            params.put("itag", (Object) "1");
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject updateMember(String userid, MemberMDL member) {
        try {
            String url = GetMethodURL("updateMember");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("memberid", member.getMemberid());
            params.put("membername", member.getMembername());
            params.put("birthday", member.getBirthday());
            params.put("height", member.getHeight());
            params.put("waist", member.getWaist());
            params.put("sex", member.getSex());
            params.put("targetweight", member.getTargetweight());
            params.put("modeltype", member.getModeltype());
            params.put("itag", (Object) "2");
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject deleteMember(String userid, MemberMDL member) {
        try {
            String url = GetMethodURL("updateMember");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("memberid", member.getMemberid());
            params.put("membername", member.getMembername());
            params.put("birthday", member.getBirthday());
            params.put("height", member.getHeight());
            params.put("waist", member.getWaist());
            params.put("sex", member.getSex());
            params.put("targetweight", member.getTargetweight());
            params.put("modeltype", member.getModeltype());
            params.put("itag", ModeTypeEnum.Baby);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getMember(String userid) {
        try {
            String url = GetMethodURL("getMember");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject updateMemberIcon(MemberMDL mdl) {
        try {
            String url = GetMethodURL("updateMemberIcon");
            RequestParams params = getParams();
            params.put("userid", mdl.getUserid());
            params.put("memberid", mdl.getMemberid());
            params.put("icondata", FileHelper.BytetoStreamString(mdl.getClientImg()));
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }
}
