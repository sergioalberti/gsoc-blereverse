package com.vc.cloudbalance.common;

import com.vc.cloudbalance.model.ModeTypeEnum;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.util.ObjectHelper;

public class WeightUnitHelper {
    public static final String Jin = "2";
    public static final String Kg = "0";
    public static final String Lb = "1";

    public static String getConvertedWtVal(boolean isHighPrecision, int count, Object val) {
        if (val == null) {
            return "";
        }
        String tmpStr = new AppConfigDAL(null).select(Constants.SQL_KEY_WEIGHT_UNIT_STRING);
        try {
            if (tmpStr.equals("1")) {
                String tempStr = ObjectHelper.Convert2MathCount(2, new StringBuilder(String.valueOf(((double) ObjectHelper.Convert2Float(val.toString())) * 2.2046d)).toString());
                if (isHighPrecision) {
                    return tempStr.substring(0, tempStr.length() - 1);
                }
                String[] tempStrings = tempStr.split("\\.");
                tempStr = tempStrings[1].substring(0, 1);
                if (tempStr.equals("1")) {
                    tempStr = "2";
                } else if (tempStr.equals(ModeTypeEnum.Baby)) {
                    tempStr = "4";
                } else if (tempStr.equals("5")) {
                    tempStr = "6";
                } else if (tempStr.equals("7")) {
                    tempStr = "8";
                } else if (tempStr.equals("9")) {
                    tempStr = Kg;
                    tempStrings[0] = new StringBuilder(String.valueOf(ObjectHelper.Convert2Int(tempStrings[0]) + 1)).toString();
                }
                return tempStrings[0] + "." + tempStr;
            } else if (tmpStr.equals("2")) {
                return ObjectHelper.Convert2MathCount(count, Float.valueOf(ObjectHelper.Convert2Float(val.toString()) * 2.0f));
            } else {
                tmpStr = ObjectHelper.Convert2MathCount(count, val.toString());
                return ObjectHelper.Convert2MathCount(count, val.toString());
            }
        } catch (Exception e) {
            return val.toString();
        }
    }

    public static String getConvertedWtVal(int count, Object val) {
        if (val == null) {
            return "";
        }
        String tmpStr = new AppConfigDAL(null).select(Constants.SQL_KEY_WEIGHT_UNIT_STRING);
        try {
            if (tmpStr.equals("1")) {
                return ObjectHelper.Convert2MathCount(1, Double.valueOf(((double) ObjectHelper.Convert2Float(val.toString())) * 2.2046d));
            }
            if (tmpStr.equals("2")) {
                return ObjectHelper.Convert2MathCount(count, Float.valueOf(ObjectHelper.Convert2Float(val.toString()) * 2.0f));
            }
            tmpStr = ObjectHelper.Convert2MathCount(count, val.toString());
            return ObjectHelper.Convert2MathCount(count, val.toString());
        } catch (Exception e) {
            return val.toString();
        }
    }

    public static String convert2KgVal(int count, Object val) {
        if (val == null) {
            return "";
        }
        String tmpStr = new AppConfigDAL(null).select(Constants.SQL_KEY_WEIGHT_UNIT_STRING);
        try {
            if (tmpStr.equals("1")) {
                return ObjectHelper.Convert2MathCount(count, Double.valueOf(((double) ObjectHelper.Convert2Float(val.toString())) / 2.2046d));
            }
            if (tmpStr.equals("2")) {
                return ObjectHelper.Convert2MathCount(count, Float.valueOf(ObjectHelper.Convert2Float(val.toString()) / 2.0f));
            }
            return ObjectHelper.Convert2MathCount(count, val.toString());
        } catch (Exception e) {
            return val.toString();
        }
    }

    public static String getWeightUnitString() {
        String tmpStr = new AppConfigDAL(null).select(Constants.SQL_KEY_WEIGHT_UNIT_STRING);
        if (tmpStr.equals("1")) {
            return "lb";
        }
        if (tmpStr.equals("2")) {
            return "市斤";
        }
        return "Kg";
    }
}
