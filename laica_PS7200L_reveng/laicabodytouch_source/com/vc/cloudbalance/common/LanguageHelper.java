package com.vc.cloudbalance.common;

import android.content.Context;
import com.vc.cloudbalance.model.LanguageMDL;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.sqlite.LanguageDAL;
import java.util.HashMap;
import java.util.List;

public class LanguageHelper {
    private static String LanguageVal = "";
    private static HashMap<String, LanguageMDL> Maps;
    private static List<LanguageMDL> _Languages;

    public static List<LanguageMDL> GetLanguages() {
        if (_Languages == null) {
            init();
        }
        return _Languages;
    }

    private static void init() {
        Maps = new HashMap();
    }

    public static void SetLanguage(String val) {
        LanguageVal = val;
    }

    public static String GetLanguage() {
        if (LanguageVal.equals("")) {
            LanguageVal = new AppConfigDAL(null).select(Constants.SQL_KEY_LANGUAGE_STRING);
        }
        return LanguageVal;
    }

    public static String GetVal(String key, Context mContext) {
        if (Maps == null) {
            Maps = new LanguageDAL(mContext).select();
        }
        if (Maps == null) {
            return "";
        }
        LanguageMDL language = (LanguageMDL) Maps.get(key);
        if (language == null) {
            return "";
        }
        if (LanguageVal.equals("")) {
            LanguageVal = new AppConfigDAL(mContext).select(Constants.SQL_KEY_LANGUAGE_STRING);
        }
        return language.GetVal(LanguageVal);
    }
}
