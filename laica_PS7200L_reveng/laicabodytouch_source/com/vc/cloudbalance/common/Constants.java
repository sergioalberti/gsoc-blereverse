package com.vc.cloudbalance.common;

public class Constants {
    public static final String DEVICETYPE_STRING = "DEVICETYPE_STRING";
    public static final String EXTRA_KEY_ID_STRING = "EXTRA_KEY_ID_STRING";
    public static final String EXTRA_KEY_JSON_STRING = "EXTRA_KEY_JSON_STRING";
    public static final String EXTRA_KEY_MODETYPT_STRING = "EXTRA_KEY_MODETYPT_STRING";
    public static final String SQL_KEY_BALANCETYPE_STRING = "SQL_KEY_BALANCETYPE_STRING";
    public static final String SQL_KEY_LANGUAGECHANGE_STRING = "SQL_KEY_LANGUAGECHANGE_STRING";
    public static final String SQL_KEY_LANGUAGE_STRING = "SQL_KEY_LANGUAGE_STRING";
    public static final String SQL_KEY_LASTADULT_WTMEMBER_CLIENTID_STRING = "SQL_KEY_LASTADULT_WTMEMBER_CLIENTID_STRING";
    public static final String SQL_KEY_LASTCHILD_WTMEMBER_CLIENTID_STRING = "SQL_KEY_LASTCHILD_WTMEMBER_CLIENTID_STRING";
    public static final String SQL_KEY_WEIGHT_UNIT_STRING = "SQL_KEY_WEIGHT_UNIT_STRING";
    public static final String THEME_COLOR_STRING = "THEME_COLOR_STRING";
}
