package com.vc.cloudbalance.common;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.whb.loease.bodytouch.C0181R;

public class DialogHelper {
    static Dialog dialog;
    static TextView tipTextView;

    public static void ShowLoadingDialog(Context context, String msg) {
        if (dialog == null) {
            View v = LayoutInflater.from(context).inflate(C0181R.layout.loading_dialog, null);
            LinearLayout layout = (LinearLayout) v.findViewById(C0181R.id.dialog_view);
            ImageView spaceshipImage = (ImageView) v.findViewById(C0181R.id.img);
            tipTextView = (TextView) v.findViewById(C0181R.id.tipTextView);
            spaceshipImage.startAnimation(null);
            tipTextView.setText(msg);
            dialog = new Dialog(context, C0181R.style.loading_dialog);
            dialog.setCancelable(false);
            dialog.setContentView(layout, new LayoutParams(-1, -1));
        } else {
            tipTextView.setText(msg);
        }
        dialog.show();
    }

    public static void CloseLoadingDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    public static void showTost(Context context, String mess) {
        try {
            Toast.makeText(context.getApplicationContext(), mess, 0).show();
        } catch (Exception e) {
        }
    }

    public static void showComfrimDialog(Context c, String title, String msg, String okTitle, OnClickListener okClickListener, String cancleTitle, OnClickListener cancleClickListener) {
        Builder dialog = new Builder(c).setTitle(title).setMessage(msg).setPositiveButton(okTitle, okClickListener);
        if (cancleClickListener != null) {
            dialog.setNegativeButton(cancleTitle, cancleClickListener);
        }
        dialog.show();
    }

    public static void showComfrimDialog(Context c, String title, String msg, OnClickListener okClickListener, OnClickListener cancleClickListener) {
        showComfrimDialog(c, title, msg, "确定", okClickListener, "取消", cancleClickListener);
    }
}
