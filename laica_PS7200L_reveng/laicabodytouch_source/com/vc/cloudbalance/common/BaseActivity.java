package com.vc.cloudbalance.common;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.TextView;
import java.util.Locale;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;

@EActivity
@NoTitle
public class BaseActivity extends Activity {
    @ViewById
    public Button btnBack;
    public Context mContext;
    @ViewById
    public TextView tvTitle;

    @Click({2131492873})
    public void goBack() {
        finish();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    public void InitBase() {
        ApplicationMamager.getInstance().addActivity(this);
        this.mContext = this;
        initLanguage();
    }

    public void setTitleText(String str) {
        this.tvTitle.setText(str);
    }

    public App GetApp() {
        return App.getApp(this);
    }

    private void initLanguage() {
        int LanguageVal = getSharedPreferences("AppConfigDAL", 0).getInt(Constants.SQL_KEY_LANGUAGE_STRING, 0);
        if (LanguageVal != 0) {
            try {
                Resources resources = getResources();
                Configuration config = resources.getConfiguration();
                DisplayMetrics dm = resources.getDisplayMetrics();
                if (LanguageVal == 1 || LanguageVal == 2) {
                    config.locale = new Locale("zh", Common.getLanguageShortCodes()[LanguageVal - 1]);
                } else {
                    config.locale = new Locale(Common.getLanguageShortCodes()[LanguageVal - 1]);
                }
                resources.updateConfiguration(config, dm);
                Locale.setDefault(config.locale);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
