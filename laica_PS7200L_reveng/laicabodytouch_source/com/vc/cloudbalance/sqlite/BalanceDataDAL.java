package com.vc.cloudbalance.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.vc.cloudbalance.common.App;
import com.vc.cloudbalance.common.DatabaseHelper;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.util.LogUtils;
import java.util.LinkedList;
import java.util.List;

public class BalanceDataDAL {
    Context context;
    SQLiteDatabase mDb = null;
    DatabaseHelper mDbHelper = null;
    String selectPara = "id,memberid,userid,weidate,weight,bmi,fatpercent,muscle,bone,water,basalmetabolism,innerfat,upload,clientmemberid,dataid,clientImg,height,haveimg,imgurl,babygrowth,changeHeight,changeWeight";

    public BalanceDataDAL(Context c) {
        this.context = c;
        this.mDbHelper = DatabaseHelper.getInstance(c);
        this.mDb = this.mDbHelper.getDatabase();
    }

    public BalanceDataMDL SelectLastData(String memberid, String clientid) {
        try {
            BalanceDataMDL i;
            synchronized (App.threadDBLock) {
                String sql = "select * from BalanceData where 1=1 %s  order by weidate desc limit 1 ";
                String sqlwhere = "";
                if (!TextUtils.isEmpty(memberid)) {
                    sqlwhere = new StringBuilder(String.valueOf(sqlwhere)).append(" and memberid = '").append(memberid).append("'").toString();
                } else if (!TextUtils.isEmpty(clientid)) {
                    sqlwhere = new StringBuilder(String.valueOf(sqlwhere)).append(" and clientmemberid = '").append(clientid).append("'").toString();
                }
                Cursor cursor = this.mDb.rawQuery(String.format(sql, new Object[]{sqlwhere}), new String[0]);
                i = null;
                if (cursor.moveToNext()) {
                    i = convert(cursor);
                }
                cursor.close();
            }
            return i;
        } catch (Exception e) {
            return null;
        }
    }

    public BalanceDataMDL SelectById(String id) {
        try {
            BalanceDataMDL datas;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from BalanceData  where id=? ";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{id});
                datas = null;
                if (cursor.moveToNext()) {
                    datas = convert(cursor);
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public List<BalanceDataMDL> SelectUnUploadData() {
        try {
            List<BalanceDataMDL> datas;
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select * from BalanceData where id in (select max(id) as maxid from BalanceData  group by substr(weidate,0,11)) and upload =0 order by weidate desc ", new String[0]);
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return new LinkedList();
        }
    }

    public List<BalanceDataMDL> SelectThisDateData(String memberid, String clientid, String startTime, String endTime) {
        try {
            List<BalanceDataMDL> datas;
            synchronized (App.threadDBLock) {
                String sql = "select * from BalanceData where weidate >=? and weidate<=? %s order by weidate desc ";
                String sqlwhere = "";
                if (!TextUtils.isEmpty(memberid)) {
                    sqlwhere = new StringBuilder(String.valueOf(sqlwhere)).append(" and memberid = '").append(memberid).append("'").toString();
                } else if (!TextUtils.isEmpty(clientid)) {
                    sqlwhere = new StringBuilder(String.valueOf(sqlwhere)).append(" and clientmemberid = '").append(clientid).append("'").toString();
                }
                sql = String.format(sql, new Object[]{sqlwhere});
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{startTime, endTime});
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return new LinkedList();
        }
    }

    public List<BalanceDataMDL> SelectDayDataByTime(String memberid, String clientid, String startTime, String endTime) {
        try {
            List<BalanceDataMDL> datas;
            synchronized (App.threadDBLock) {
                String sql = "select * from BalanceData where id in (select max(id) as maxid from BalanceData where 1=1 %s group by substr(weidate,0,11))  order by weidate desc ";
                String sqlwhere = "";
                if (!TextUtils.isEmpty(memberid)) {
                    sqlwhere = new StringBuilder(String.valueOf(sqlwhere)).append(" and memberid = '").append(memberid).append("'").toString();
                } else if (!TextUtils.isEmpty(clientid)) {
                    sqlwhere = new StringBuilder(String.valueOf(sqlwhere)).append(" and clientmemberid = '").append(clientid).append("'").toString();
                }
                if (!(TextUtils.isEmpty(startTime) || TextUtils.isEmpty(endTime))) {
                    sqlwhere = new StringBuilder(String.valueOf(sqlwhere)).append(" and weidate >= '").append(startTime).append("' and weidate< '").append(endTime).append("' ").toString();
                }
                Cursor cursor = this.mDb.rawQuery(String.format(sql, new Object[]{sqlwhere}), new String[0]);
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return new LinkedList();
        }
    }

    public int SelectCountById(String memberId) {
        try {
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select  count(*) from BalanceData where memberid =? group by substr(weidate,0,11) ", new String[]{memberId});
                if (cursor.moveToNext()) {
                    int i = cursor.getInt(0);
                    return i;
                }
                cursor.close();
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public List<BalanceDataMDL> SelectByMemberId(String memberId) {
        try {
            List<BalanceDataMDL> datas;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from BalanceData  where memberid=? order by weidate desc";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{memberId});
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return new LinkedList();
        }
    }

    public List<BalanceDataMDL> SelectByMemberId(String memberId, int pageIndex) {
        try {
            List<BalanceDataMDL> datas;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from BalanceData  where memberid=? order by weidate desc limit ?,?";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{memberId, new StringBuilder(String.valueOf((pageIndex - 1) * 10)).toString(), "10"});
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return new LinkedList();
        }
    }

    public List<BalanceDataMDL> SelectByClientMemberId(String clientmemberId) {
        try {
            List<BalanceDataMDL> datas;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from BalanceData  where clientmemberId=?  order by weidate desc";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{clientmemberId});
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public List<BalanceDataMDL> SelectByClientMemberId(String clientmemberId, int pageIndex) {
        try {
            List<BalanceDataMDL> datas;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from BalanceData  where clientmemberId=?  order by weidate desc limit ?,?";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{clientmemberId, new StringBuilder(String.valueOf((pageIndex - 1) * 10)).toString(), "10"});
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public List<BalanceDataMDL> SelectUnUpload() {
        try {
            List<BalanceDataMDL> datas;
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select  " + this.selectPara + " from BalanceData  where upload=0 and memberid !='' and memberid is not null ", new String[0]);
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    private BalanceDataMDL convert(Cursor cursor) {
        BalanceDataMDL i = new BalanceDataMDL();
        try {
            i.setId(cursor.getInt(0));
            i.setMemberid(cursor.getString(1));
            i.setUserid(cursor.getString(2));
            i.setWeidateString(cursor.getString(3));
            i.setWeight(cursor.getString(4));
            i.setBmi(cursor.getString(5));
            i.setFatpercent(cursor.getString(6));
            i.setMuscle(cursor.getString(7));
            i.setBone(cursor.getString(8));
            i.setWater(cursor.getString(9));
            i.setBasalmetabolism(cursor.getString(10));
            i.setInnerfat(cursor.getString(11));
            i.setUpload(cursor.getInt(12));
            i.setClientmemberid(cursor.getString(13));
            i.setDataid(cursor.getString(14));
            i.setClientImg(cursor.getBlob(15));
            i.setHeight(cursor.getString(16));
            i.setHaveimg(cursor.getString(17));
            i.setPicurl(cursor.getString(18));
            i.setBabyGrowth(cursor.getString(19));
            i.setchangeHeight(cursor.getString(20));
            i.setchangeWeight(cursor.getString(21));
        } catch (Exception e) {
        }
        return i;
    }

    public Boolean UpdateUnloadData(BalanceDataMDL data) {
        try {
            Boolean valueOf;
            synchronized (App.threadDBLock) {
                String sql = "update BalanceData set upload=1 where id=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{Integer.valueOf(data.getId())});
                    valueOf = Boolean.valueOf(true);
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return Boolean.valueOf(false);
                }
            }
            return valueOf;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return Boolean.valueOf(false);
        }
    }

    public Boolean setUnloadData(BalanceDataMDL data) {
        try {
            Boolean valueOf;
            synchronized (App.threadDBLock) {
                String sql = "update BalanceData set upload=0 where id=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{Integer.valueOf(data.getId())});
                    valueOf = Boolean.valueOf(true);
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return Boolean.valueOf(false);
                }
            }
            return valueOf;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return Boolean.valueOf(false);
        }
    }

    public boolean Insert(BalanceDataMDL data) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "insert into BalanceData (" + this.selectPara + ") values (null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                try {
                    this.mDb.execSQL(sql, new Object[]{data.getMemberid(), data.getUserid(), data.getWeidateString(), data.getWeight(), data.getBmi(), data.getFatpercent(), data.getMuscle(), data.getBone(), data.getWater(), data.getBasalmetabolism(), data.getInnerfat(), Integer.valueOf(data.getUpload()), data.getClientmemberid(), data.getDataid(), data.getClientImg(), data.getHeight(), data.getHaveimg(), data.getPicurl(), data.getBabyGrowth(), data.getchangeHeight(), data.getchangeWeight()});
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean DelById(int id) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "delete from BalanceData where id=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{Integer.valueOf(id)});
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean DelByMemberId(String memberid) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "delete from BalanceData where memberid=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{memberid});
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean UpdateImage(BalanceDataMDL mdl) {
        try {
            synchronized (App.threadDBLock) {
                this.mDb.beginTransaction();
                try {
                    this.mDb.execSQL("update BalanceData set  clientImg=? where clientid=?", new Object[]{mdl.getClientImg(), Integer.valueOf(mdl.getId())});
                    this.mDb.setTransactionSuccessful();
                    this.mDb.endTransaction();
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    this.mDb.endTransaction();
                    return false;
                } catch (Throwable th) {
                    this.mDb.endTransaction();
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean Insert(MemberMDL member, List<BalanceDataMDL> balancedatas) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "insert into BalanceData (" + this.selectPara + ") values (null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                try {
                    this.mDb.beginTransaction();
                    this.mDb.execSQL("delete from BalanceData where memberid= ?", new Object[]{member.getMemberid()});
                    for (BalanceDataMDL data : balancedatas) {
                        this.mDb.execSQL(sql, new Object[]{member.getMemberid(), member.getUserid(), data.getWeidateString(), data.getWeight(), data.getBmi(), data.getFatpercent(), data.getMuscle(), data.getBone(), data.getWater(), data.getBasalmetabolism(), data.getInnerfat(), Integer.valueOf(1), data.getClientmemberid(), data.getDataid(), data.getClientImg(), data.getHeight(), data.getHaveimg(), data.getPicurl(), data.getBabyGrowth(), data.getchangeHeight(), data.getchangeWeight()});
                    }
                    this.mDb.setTransactionSuccessful();
                    this.mDb.endTransaction();
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    this.mDb.endTransaction();
                    return false;
                } catch (Throwable th) {
                    this.mDb.endTransaction();
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public int GetDataListSizeByMemberId(String memberId) {
        try {
            int DataSize;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from BalanceData  where memberid=? order by weidate desc";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{memberId});
                DataSize = 0;
                while (cursor.moveToNext()) {
                    DataSize++;
                }
                cursor.close();
            }
            return DataSize;
        } catch (Exception e) {
            LogUtils.m3e(e.toString());
            return 0;
        }
    }

    public int GetDataListSizeByClientMemberId(String clientmemberId) {
        try {
            int DataSize;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from BalanceData  where clientmemberId=? order by weidate desc";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{clientmemberId});
                DataSize = 0;
                while (cursor.moveToNext()) {
                    DataSize++;
                }
                cursor.close();
            }
            return DataSize;
        } catch (Exception e) {
            LogUtils.m3e(e.toString());
            return 0;
        }
    }
}
