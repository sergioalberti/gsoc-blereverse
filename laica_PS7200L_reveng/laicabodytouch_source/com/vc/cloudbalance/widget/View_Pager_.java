package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class View_Pager_ extends View_Pager implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    public View_Pager_(Context context) {
        super(context);
        init_();
    }

    public View_Pager_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static View_Pager build(Context context) {
        View_Pager_ instance = new View_Pager_(context);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            inflate(getContext(), C0181R.layout.view_pager, this);
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static View_Pager build(Context context, AttributeSet attrs) {
        View_Pager_ instance = new View_Pager_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.pbLoading = (ProgressBar) hasViews.findViewById(C0181R.id.pbLoading);
        this.tvLoading = (TextView) hasViews.findViewById(C0181R.id.tvLoading);
    }
}
