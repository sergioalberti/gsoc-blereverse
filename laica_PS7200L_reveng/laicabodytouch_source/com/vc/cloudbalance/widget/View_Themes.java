package com.vc.cloudbalance.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(2130903081)
public class View_Themes extends LinearLayout {
    @ViewById
    Button btnConfirm;
    private int currentTheme = 0;
    @ViewById
    ImageView imgBlueThemeSelected;
    @ViewById
    ImageView imgGreenThemeSelected;
    @ViewById
    ImageView imgGreyThemeSelected;
    @ViewById
    ImageView imgOrangeThemeSelected;
    @ViewById
    LinearLayout llGreenTheme;
    Context mContext;
    PopupWindow mPopupWindow;
    private int theme = 0;

    public View_Themes(Context context, PopupWindow popupWindow) {
        super(context);
        this.mContext = context;
        this.mPopupWindow = popupWindow;
    }

    public View_Themes(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    @Click({2131493034})
    void confirm() {
        if (this.theme == 0) {
            this.theme = 3;
        }
        new AppConfigDAL(this.mContext).insert(Constants.THEME_COLOR_STRING, new StringBuilder(String.valueOf(this.theme)).toString());
        if (this.currentTheme != this.theme) {
            new AppConfigDAL(this.mContext).insert(Constants.THEME_COLOR_STRING, new StringBuilder(String.valueOf(this.theme)).toString());
            initThemeView();
            if (!((Activity) this.mContext).getLocalClassName().contains("AppConfig_NullActivity_")) {
                ((Activity) this.mContext).finish();
                this.mContext.startActivity(((Activity) this.mContext).getIntent());
            }
        }
        if (this.mPopupWindow != null) {
            this.mPopupWindow.dismiss();
        }
    }

    @Click({2131493102})
    void select1() {
        setSelectedView(2);
    }

    @Click({2131493108})
    void select2() {
        setSelectedView(4);
    }

    @Click({2131493105})
    void select3() {
        setSelectedView(1);
    }

    @Click({2131493111})
    void select4() {
        setSelectedView(3);
    }

    @AfterViews
    void init() {
        this.theme = Common.getThemeType();
        this.currentTheme = this.theme;
        setSelectedView(this.theme);
        initThemeView();
    }

    private void setSelectedView(int themeType) {
        this.theme = themeType;
        this.imgBlueThemeSelected.setVisibility(8);
        this.imgGreenThemeSelected.setVisibility(8);
        this.imgGreyThemeSelected.setVisibility(8);
        this.imgOrangeThemeSelected.setVisibility(8);
        if (themeType == 1) {
            this.imgOrangeThemeSelected.setVisibility(0);
        } else if (themeType == 2) {
            this.imgGreenThemeSelected.setVisibility(0);
        } else if (themeType == 4) {
            this.imgGreyThemeSelected.setVisibility(0);
        } else if (themeType == 3) {
            this.imgBlueThemeSelected.setVisibility(0);
        }
    }

    private void initThemeView() {
        switch (Common.getThemeType()) {
            case 1:
                this.btnConfirm.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_orange_selector);
                this.btnConfirm.setTextColor(this.mContext.getResources().getColorStateList(C0181R.drawable.colorselector_orange_2_white));
                return;
            case 2:
                this.btnConfirm.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_green_selector);
                this.btnConfirm.setTextColor(this.mContext.getResources().getColorStateList(C0181R.drawable.colorselector_green_2_white));
                return;
            case 4:
                this.btnConfirm.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_grey_selector);
                this.btnConfirm.setTextColor(this.mContext.getResources().getColorStateList(C0181R.drawable.colorselector_grey_2_white));
                return;
            default:
                this.btnConfirm.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_blue_selector);
                this.btnConfirm.setTextColor(this.mContext.getResources().getColorStateList(C0181R.drawable.colorselector_blue_2_white));
                return;
        }
    }
}
