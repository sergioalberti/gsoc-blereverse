package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vc.cloudbalance.model.MemberMDL;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class BabyBalanceView_ extends BabyBalanceView implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C00921 implements OnClickListener {
        C00921() {
        }

        public void onClick(View view) {
            BabyBalanceView_.this.saveData();
        }
    }

    class C00932 implements OnClickListener {
        C00932() {
        }

        public void onClick(View view) {
            BabyBalanceView_.this.notSaveData();
        }
    }

    class C00943 implements OnClickListener {
        C00943() {
        }

        public void onClick(View view) {
            BabyBalanceView_.this.back();
        }
    }

    public BabyBalanceView_(Context context, MemberMDL member, PopupWindow popupWindow) {
        super(context, member, popupWindow);
        init_();
    }

    public BabyBalanceView_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static BabyBalanceView build(Context context, MemberMDL member, PopupWindow popupWindow) {
        BabyBalanceView_ instance = new BabyBalanceView_(context, member, popupWindow);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static BabyBalanceView build(Context context, AttributeSet attrs) {
        BabyBalanceView_ instance = new BabyBalanceView_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.tvBabyWtVal = (TextView) hasViews.findViewById(C0181R.id.tvBabyWtVal);
        this.llMiddle = (LinearLayout) hasViews.findViewById(C0181R.id.llMiddle);
        this.imgMom = (RadioButton) hasViews.findViewById(C0181R.id.imgMom);
        this.tvMsg = (TextView) hasViews.findViewById(C0181R.id.tvMsg);
        this.tvTitle = (TextView) hasViews.findViewById(C0181R.id.tvTitle);
        this.btnConfirm = (Button) hasViews.findViewById(C0181R.id.btnConfirm);
        this.tvMomBBWtVal = (TextView) hasViews.findViewById(C0181R.id.tvMomBBWtVal);
        this.tvMomWtVal = (TextView) hasViews.findViewById(C0181R.id.tvMomWtVal);
        this.tvWtUnit1 = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit1);
        this.rlWtCircle = (RelativeLayout) hasViews.findViewById(C0181R.id.rlWtCircle);
        this.tvWtUnit3 = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit3);
        this.imgBaby = (RadioButton) hasViews.findViewById(C0181R.id.imgBaby);
        this.tvWtUnit4 = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit4);
        this.tvWtUnit2 = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit2);
        this.tvWeightVal = (TextView) hasViews.findViewById(C0181R.id.tvWeightVal);
        this.btnCancel = (Button) hasViews.findViewById(C0181R.id.btnCancel);
        this.imgMomAndBB = (RadioButton) hasViews.findViewById(C0181R.id.imgMomAndBB);
        if (this.btnConfirm != null) {
            this.btnConfirm.setOnClickListener(new C00921());
        }
        if (this.btnCancel != null) {
            this.btnCancel.setOnClickListener(new C00932());
        }
        View view = hasViews.findViewById(C0181R.id.btnBack);
        if (view != null) {
            view.setOnClickListener(new C00943());
        }
        init();
    }
}
