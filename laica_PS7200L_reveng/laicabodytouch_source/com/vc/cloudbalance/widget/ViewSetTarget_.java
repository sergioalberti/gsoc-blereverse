package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import com.vc.cloudbalance.model.MemberMDL;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class ViewSetTarget_ extends ViewSetTarget implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01041 implements OnClickListener {
        C01041() {
        }

        public void onClick(View view) {
            ViewSetTarget_.this.setFinishTime();
        }
    }

    class C01052 implements OnClickListener {
        C01052() {
        }

        public void onClick(View view) {
            ViewSetTarget_.this.saveTarget();
        }
    }

    class C01063 implements OnClickListener {
        C01063() {
        }

        public void onClick(View view) {
            ViewSetTarget_.this.setStartTime();
        }
    }

    public ViewSetTarget_(Context context, MemberMDL member) {
        super(context, member);
        init_();
    }

    public ViewSetTarget_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static ViewSetTarget build(Context context, MemberMDL member) {
        ViewSetTarget_ instance = new ViewSetTarget_(context, member);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static ViewSetTarget build(Context context, AttributeSet attrs) {
        ViewSetTarget_ instance = new ViewSetTarget_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.tvFinishTime = (TextView) hasViews.findViewById(C0181R.id.tvFinishTime);
        this.tvWtUnit2 = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit2);
        this.tvCurrentWtVal = (TextView) hasViews.findViewById(C0181R.id.tvCurrentWtVal);
        this.tvTargetWtVal = (EditText) hasViews.findViewById(C0181R.id.tvTargetWtVal);
        this.tvWtUnit1 = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit1);
        this.tvStartTime = (TextView) hasViews.findViewById(C0181R.id.tvStartTime);
        if (this.tvFinishTime != null) {
            this.tvFinishTime.setOnClickListener(new C01041());
        }
        View view = hasViews.findViewById(C0181R.id.btnConfirm);
        if (view != null) {
            view.setOnClickListener(new C01052());
        }
        if (this.tvStartTime != null) {
            this.tvStartTime.setOnClickListener(new C01063());
        }
        init();
    }
}
