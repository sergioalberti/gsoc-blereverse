package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import com.vc.cloudbalance.model.MemberMDL;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class ViewGuestMemberInfo_ extends ViewGuestMemberInfo implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01001 implements OnClickListener {
        C01001() {
        }

        public void onClick(View view) {
            ViewGuestMemberInfo_.this.cancel();
        }
    }

    class C01012 implements OnClickListener {
        C01012() {
        }

        public void onClick(View view) {
            ViewGuestMemberInfo_.this.save();
        }
    }

    public ViewGuestMemberInfo_(Context context, PopupWindow popupWindow, MemberMDL member) {
        super(context, popupWindow, member);
        init_();
    }

    public ViewGuestMemberInfo_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static ViewGuestMemberInfo build(Context context, PopupWindow popupWindow, MemberMDL member) {
        ViewGuestMemberInfo_ instance = new ViewGuestMemberInfo_(context, popupWindow, member);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            inflate(getContext(), C0181R.layout.view_guestmember_info, this);
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static ViewGuestMemberInfo build(Context context, AttributeSet attrs) {
        ViewGuestMemberInfo_ instance = new ViewGuestMemberInfo_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.line2 = hasViews.findViewById(C0181R.id.line2);
        this.btnConfirm = (Button) hasViews.findViewById(C0181R.id.btnConfirm);
        this.btnCancel = (Button) hasViews.findViewById(C0181R.id.btnCancel);
        this.rbGenderFeman = (RadioButton) hasViews.findViewById(C0181R.id.rbGenderFeman);
        this.etSex = (EditText) hasViews.findViewById(C0181R.id.etSex);
        this.tbGender = (TableRow) hasViews.findViewById(C0181R.id.tbGender);
        this.rbgGender = (RadioGroup) hasViews.findViewById(C0181R.id.rbgGender);
        this.etHeight = (EditText) hasViews.findViewById(C0181R.id.etHeight);
        this.etAge = (EditText) hasViews.findViewById(C0181R.id.etAge);
        this.rbGenderMan = (RadioButton) hasViews.findViewById(C0181R.id.rbGenderMan);
        this.line1 = hasViews.findViewById(C0181R.id.line1);
        this.tbAge = (TableRow) hasViews.findViewById(C0181R.id.tbAge);
        this.line3 = hasViews.findViewById(C0181R.id.line3);
        this.tbHeight = (TableRow) hasViews.findViewById(C0181R.id.tbHeight);
        if (this.btnCancel != null) {
            this.btnCancel.setOnClickListener(new C01001());
        }
        if (this.btnConfirm != null) {
            this.btnConfirm.setOnClickListener(new C01012());
        }
        init();
    }
}
