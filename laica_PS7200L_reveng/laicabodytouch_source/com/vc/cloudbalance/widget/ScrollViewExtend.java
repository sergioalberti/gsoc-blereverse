package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

public class ScrollViewExtend extends ScrollView {
    private float downY = 0.0f;
    GestureDetector gestureDetector;
    boolean isTouchChildren = false;
    private OnScrollViewBorderListener onScrollViewBorderListener;

    public interface OnScrollViewBorderListener {
        void onBottom();

        void onTop();
    }

    public ScrollViewExtend(Context context) {
        super(context);
    }

    public ScrollViewExtend(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setGestureDetector(GestureDetector gestureDetector) {
        this.gestureDetector = gestureDetector;
    }

    public void IsTouchingChildren() {
        this.isTouchChildren = true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.isTouchChildren) {
            getParent().requestDisallowInterceptTouchEvent(false);
            return super.onTouchEvent(event);
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        super.onTouchEvent(event);
        return this.gestureDetector.onTouchEvent(event);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == 0) {
            this.isTouchChildren = false;
        }
        if (this.isTouchChildren) {
            getParent().requestDisallowInterceptTouchEvent(false);
            return super.dispatchTouchEvent(ev);
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        this.gestureDetector.onTouchEvent(ev);
        super.dispatchTouchEvent(ev);
        return true;
    }

    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        doOnBorderListener();
    }

    public void setOnScrollViewBorderListener(OnScrollViewBorderListener onScrollViewBorderListener) {
        this.onScrollViewBorderListener = onScrollViewBorderListener;
    }

    private void doOnBorderListener() {
        View contentView = getChildAt(0);
        if (contentView == null || contentView.getMeasuredHeight() > getScrollY() + getHeight()) {
            if (getScrollY() == 0 && this.onScrollViewBorderListener != null) {
                this.onScrollViewBorderListener.onTop();
            }
        } else if (this.onScrollViewBorderListener != null) {
            this.onScrollViewBorderListener.onBottom();
        }
    }
}
