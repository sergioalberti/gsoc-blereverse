package com.vc.cloudbalance.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.HorizontalScrollView;
import android.widget.OverScroller;
import android.widget.Scroller;
import java.lang.reflect.Field;

public class LockableHorizontalScrollView extends HorizontalScrollView {
    static final int ANIMATED_SCROLL_GAP = 250;
    public static boolean test = false;
    private int downX = 0;
    private long down_time;
    boolean isCancel = false;
    private long mLastScroll;
    private boolean mScrollable = true;
    private Field mScrollerField;
    int nowX = 0;
    OnCustomTouchListenter onCustomTouchListenter;
    int screenWidth = 0;
    private int scroll_x = 0;
    ScrollerEx scrollerEx = null;

    public interface OnCustomTouchListenter {
        void click();

        void close();

        void open();

        void prePage();

        void touch();
    }

    private interface ScrollerEx {
        void abortAnimation();

        void create(Context context, Interpolator interpolator);

        Object getScroller();

        boolean isFinished();

        void startScroll(int i, int i2, int i3, int i4, int i5);
    }

    class C02641 implements ScrollerEx {
        private OverScroller mScroller = null;

        C02641() {
        }

        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            this.mScroller.startScroll(startX, startY, dx, dy, duration);
        }

        public boolean isFinished() {
            return this.mScroller.isFinished();
        }

        public Object getScroller() {
            return this.mScroller;
        }

        @SuppressLint({"NewApi"})
        public void create(Context context, Interpolator interpolator) {
            this.mScroller = new OverScroller(context, interpolator);
        }

        public void abortAnimation() {
            if (this.mScroller != null) {
                this.mScroller.abortAnimation();
            }
        }
    }

    class C02652 implements ScrollerEx {
        private Scroller mScroller = null;

        C02652() {
        }

        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            this.mScroller.startScroll(startX, startY, dx, dy, duration);
        }

        public boolean isFinished() {
            return this.mScroller.isFinished();
        }

        public Object getScroller() {
            return this.mScroller;
        }

        public void create(Context context, Interpolator interpolator) {
            this.mScroller = new Scroller(context, interpolator);
        }

        public void abortAnimation() {
            if (this.mScroller != null) {
                this.mScroller.abortAnimation();
            }
        }
    }

    public LockableHorizontalScrollView(Context context, AttributeSet attrset) {
        super(context, attrset);
        this.screenWidth = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
        initScroller();
    }

    public OnCustomTouchListenter getOnCustomTouchListenter() {
        return this.onCustomTouchListenter;
    }

    public void setOnCustomTouchListenter(OnCustomTouchListenter onCustomTouchListenter) {
        this.onCustomTouchListenter = onCustomTouchListenter;
    }

    public int getScroll_x() {
        return this.scroll_x;
    }

    public void setScroll_x(int scroll_x) {
        this.scroll_x = scroll_x;
    }

    public void setIsScrollable(boolean scrollable) {
        this.mScrollable = scrollable;
    }

    public boolean getIsScrollable() {
        return this.mScrollable;
    }

    private void initScroller() {
        try {
            this.mScrollerField = HorizontalScrollView.class.getDeclaredField("mScroller");
            this.mScrollerField.setAccessible(true);
            if ("OverScroller".equals(this.mScrollerField.getType().getSimpleName())) {
                this.scrollerEx = new C02641();
            } else {
                this.scrollerEx = new C02652();
            }
        } catch (Exception e) {
        }
    }

    public final void smoothScrollBy(int dx, int dy, int addDuration) {
        this.scrollerEx.abortAnimation();
        this.scrollerEx.create(getContext(), new OvershootInterpolator(0.0f));
        try {
            this.mScrollerField.set(this, this.scrollerEx.getScroller());
        } catch (Exception e) {
        }
        if (AnimationUtils.currentAnimationTimeMillis() - this.mLastScroll > 250) {
            this.scrollerEx.startScroll(getScrollX(), getScrollY(), dx, dy, addDuration);
            awakenScrollBars();
            invalidate();
        } else {
            if (!this.scrollerEx.isFinished()) {
                this.scrollerEx.abortAnimation();
            }
            scrollBy(dx, dy);
        }
        this.mLastScroll = AnimationUtils.currentAnimationTimeMillis();
    }

    public final void smoothScrollTo(int x, int y, int duration) {
        smoothScrollBy(x - getScrollX(), y - getScrollY(), duration);
    }

    public boolean IsOpen() {
        if (this.nowX == 0) {
            return false;
        }
        return true;
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this.onCustomTouchListenter != null) {
            this.onCustomTouchListenter.touch();
        }
        return super.dispatchTouchEvent(ev);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r10) {
        /*
        r9 = this;
        r8 = 550; // 0x226 float:7.71E-43 double:2.717E-321;
        r3 = 1;
        r2 = 0;
        r4 = "LockableHorizontalScrollView->onTouchEvent";
        r5 = "";
        android.util.Log.e(r4, r5);
        r4 = r10.getAction();
        switch(r4) {
            case 0: goto L_0x0017;
            case 1: goto L_0x0044;
            case 2: goto L_0x002e;
            case 3: goto L_0x008e;
            default: goto L_0x0012;
        };
    L_0x0012:
        r2 = super.onTouchEvent(r10);
    L_0x0016:
        return r2;
    L_0x0017:
        r3 = r10.getX();
        r3 = (int) r3;
        r9.downX = r3;
        r3 = new java.util.Date;
        r3.<init>();
        r4 = r3.getTime();
        r9.down_time = r4;
        r9.isCancel = r2;
        test = r2;
        goto L_0x0012;
    L_0x002e:
        r4 = r9.downX;
        r4 = (float) r4;
        r5 = r10.getX();
        r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1));
        if (r4 >= 0) goto L_0x0012;
    L_0x0039:
        r4 = r9.IsOpen();
        if (r4 != 0) goto L_0x0012;
    L_0x003f:
        r9.isCancel = r3;
        test = r3;
        goto L_0x0016;
    L_0x0044:
        r4 = new java.util.Date;
        r4.<init>();
        r0 = r4.getTime();
        r4 = r10.getX();
        r5 = r9.downX;
        r5 = (float) r5;
        r4 = r4 - r5;
        r4 = java.lang.Math.abs(r4);
        r5 = 1109393408; // 0x42200000 float:40.0 double:5.481131706E-315;
        r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1));
        if (r4 > 0) goto L_0x008e;
    L_0x005f:
        r4 = r9.IsOpen();
        if (r4 == 0) goto L_0x007b;
    L_0x0065:
        r4 = r9.IsOpen();
        if (r4 == 0) goto L_0x008e;
    L_0x006b:
        r4 = r10.getX();
        r5 = r9.screenWidth;
        r6 = r9.getScroll_x();
        r5 = r5 - r6;
        r5 = (float) r5;
        r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1));
        if (r4 >= 0) goto L_0x008e;
    L_0x007b:
        r4 = r9.down_time;
        r4 = r0 - r4;
        r6 = 200; // 0xc8 float:2.8E-43 double:9.9E-322;
        r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
        if (r4 >= 0) goto L_0x008e;
    L_0x0085:
        r4 = r9.onCustomTouchListenter;
        if (r4 == 0) goto L_0x008e;
    L_0x0089:
        r4 = r9.onCustomTouchListenter;
        r4.click();
    L_0x008e:
        r4 = r10.getX();
        r5 = r9.downX;
        r5 = (float) r5;
        r4 = r4 - r5;
        r4 = java.lang.Math.abs(r4);
        r5 = 1112014848; // 0x42480000 float:50.0 double:5.49408334E-315;
        r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1));
        if (r4 <= 0) goto L_0x00e2;
    L_0x00a0:
        r4 = r10.getX();
        r5 = r9.downX;
        r5 = (float) r5;
        r4 = r4 - r5;
        r5 = 0;
        r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1));
        if (r4 <= 0) goto L_0x00cb;
    L_0x00ad:
        r4 = r9.nowX;
        if (r4 != 0) goto L_0x00ba;
    L_0x00b1:
        r4 = r9.onCustomTouchListenter;
        if (r4 == 0) goto L_0x00ba;
    L_0x00b5:
        r4 = r9.onCustomTouchListenter;
        r4.prePage();
    L_0x00ba:
        r9.nowX = r2;
        r9.smoothScrollTo(r2, r2, r8);
        r2 = r9.onCustomTouchListenter;
        if (r2 == 0) goto L_0x00c8;
    L_0x00c3:
        r2 = r9.onCustomTouchListenter;
        r2.close();
    L_0x00c8:
        r2 = r3;
        goto L_0x0016;
    L_0x00cb:
        r4 = r9.getScroll_x();
        r9.nowX = r4;
        r4 = r9.getScroll_x();
        r9.smoothScrollTo(r4, r2, r8);
        r2 = r9.onCustomTouchListenter;
        if (r2 == 0) goto L_0x00c8;
    L_0x00dc:
        r2 = r9.onCustomTouchListenter;
        r2.open();
        goto L_0x00c8;
    L_0x00e2:
        r4 = r9.nowX;
        r9.smoothScrollTo(r4, r2, r8);
        goto L_0x00c8;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vc.cloudbalance.widget.LockableHorizontalScrollView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (this.mScrollable) {
            return super.onInterceptTouchEvent(ev);
        }
        return false;
    }
}
