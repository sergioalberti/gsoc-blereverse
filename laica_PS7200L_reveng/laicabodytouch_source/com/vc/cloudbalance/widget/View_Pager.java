package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(2130903076)
public class View_Pager extends LinearLayout {
    boolean _isLoading = false;
    Context mContext;
    @ViewById
    ProgressBar pbLoading;
    @ViewById
    TextView tvLoading;

    public View_Pager(Context context) {
        super(context);
        this.mContext = context;
    }

    public View_Pager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public boolean isLoading() {
        return this._isLoading;
    }

    public void setLoading() {
        this._isLoading = true;
        this.pbLoading.setVisibility(0);
    }

    public void setHideLoading() {
        this._isLoading = false;
        this.pbLoading.setVisibility(8);
    }
}
