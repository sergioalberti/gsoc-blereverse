package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import com.vc.cloudbalance.model.MemberMDL;
import com.whb.loease.activity.DataViewActivity;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class View_DataList_Adult_ extends View_DataList_Adult implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    public View_DataList_Adult_(Context context, MemberMDL m, DataViewActivity parentView) {
        super(context, m, parentView);
        init_();
    }

    public View_DataList_Adult_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static View_DataList_Adult build(Context context, MemberMDL m, DataViewActivity parentView) {
        View_DataList_Adult_ instance = new View_DataList_Adult_(context, m, parentView);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static View_DataList_Adult build(Context context, AttributeSet attrs) {
        View_DataList_Adult_ instance = new View_DataList_Adult_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.tvMuscleVal = (TextView) hasViews.findViewById(C0181R.id.tvMuscleVal);
        this.tvBoneVal = (TextView) hasViews.findViewById(C0181R.id.tvBoneVal);
        this.tbDetail = (TableLayout) hasViews.findViewById(C0181R.id.tbDetail);
        this.tvWtUnit = (TextView) hasViews.findViewById(C0181R.id.tvWtUnit);
        this.tvFatVal = (TextView) hasViews.findViewById(C0181R.id.tvFatVal);
        this.tvWaterVal = (TextView) hasViews.findViewById(C0181R.id.tvWaterVal);
        this.tvBMRVal = (TextView) hasViews.findViewById(C0181R.id.tvBMRVal);
        this.tvBMIVal = (TextView) hasViews.findViewById(C0181R.id.tvBMIVal);
        this.llDataList = (LinearLayout) hasViews.findViewById(C0181R.id.llDataList);
        this.scrollView = (ScrollViewExtend) hasViews.findViewById(C0181R.id.scrollView);
        init();
    }
}
