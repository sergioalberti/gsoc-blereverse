package com.vc.pulltorefresh;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.vc.pulltorefresh.PullToRefreshBase.Mode;

public class PullToRefreshListView extends PullToRefreshAdapterViewBase<ListView> {
    private ListView listView;
    private LoadingLayout mHeaderLoadingView;
    private FrameLayout mLvFooterLoadingFrame;
    private FootLoadingLayout mNewFootLoadingView;

    protected class InternalListView extends ListView implements EmptyViewMethodAccessor {
        private boolean mAddedLvFooter;
        final /* synthetic */ PullToRefreshListView this$0;

        public InternalListView(PullToRefreshListView pullToRefreshListView, Context context, AttributeSet attributeSet) {
            throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
        }

        public void draw(Canvas canvas) {
            throw new Error("Unresolved compilation problem: \n");
        }

        public ContextMenuInfo getContextMenuInfo() {
            throw new Error("Unresolved compilation problem: \n");
        }

        public void setAdapter(ListAdapter listAdapter) {
            throw new Error("Unresolved compilation problem: \n");
        }

        public void setEmptyView(View view) {
            throw new Error("Unresolved compilation problem: \n");
        }

        public void setEmptyViewInternal(View view) {
            throw new Error("Unresolved compilation problem: \n");
        }
    }

    @TargetApi(9)
    final class InternalListViewSDK9 extends InternalListView {
        public InternalListViewSDK9(PullToRefreshListView pullToRefreshListView, Context context, AttributeSet attributeSet) {
            throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
        }

        protected boolean overScrollBy(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z) {
            throw new Error("Unresolved compilation problem: \n");
        }
    }

    public PullToRefreshListView(Context context) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
    }

    public PullToRefreshListView(Context context, AttributeSet attributeSet) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
    }

    public PullToRefreshListView(Context context, Mode mode) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
    }

    public ContextMenuInfo getContextMenuInfo() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setPageIndex(int i) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public int getPageIndex() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setPullLabel(String str, Mode mode) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setRefreshingLabel(String str, Mode mode) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setReleaseLabel(String str, Mode mode) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected ListView createListView(Context context, AttributeSet attributeSet) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final ListView createRefreshableView(Context context, AttributeSet attributeSet) {
        throw new Error("Unresolved compilation problem: \n\tR cannot be resolved to a variable\n");
    }

    public void setDivider(Drawable drawable) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public ListView getlistView() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setDividerHeight(int i) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void resetHeader() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void setRefreshingInternal(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void hideFootView() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void showFootView() {
        throw new Error("Unresolved compilation problem: \n");
    }
}
