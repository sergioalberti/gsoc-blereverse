package com.vc.pulltorefresh;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.vc.pulltorefresh.PullToRefreshBase.Mode;

public class LoadingLayout extends FrameLayout {
    static final int DEFAULT_ROTATION_ANIMATION_DURATION = 1200;
    private ImageView mArrowImageView;
    private final Matrix mHeaderImageMatrix;
    private final ProgressBar mHeaderProgressBar;
    private final TextView mHeaderText;
    private final Animation mImageResetRotateAnimation;
    private final Animation mImageRotateAnimation;
    private String mPullLabel;
    private String mRefreshingLabel;
    private String mReleaseLabel;
    private final Animation mRotateAnimation;
    private float mRotationPivotX;
    private float mRotationPivotY;
    private final TextView mSubHeaderText;

    public LoadingLayout(Context context, Mode mode, TypedArray typedArray) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
    }

    public void reset() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void releaseToRefresh() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setPullLabel(String str) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void refreshing() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setRefreshingLabel(String str) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setReleaseLabel(String str) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void pullToRefresh() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setTextColor(ColorStateList colorStateList) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setSubTextColor(ColorStateList colorStateList) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setTextColor(int i) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setLoadingDrawable(Drawable drawable) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setSubTextColor(int i) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setSubHeaderText(CharSequence charSequence) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void onPullY(float f) {
        throw new Error("Unresolved compilation problem: \n");
    }

    private void resetImageRotation() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private CharSequence wrapHtmlLabel(String str) {
        throw new Error("Unresolved compilation problem: \n");
    }
}
