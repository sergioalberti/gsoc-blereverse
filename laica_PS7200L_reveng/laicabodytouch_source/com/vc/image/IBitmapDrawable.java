package com.vc.image;

import android.graphics.Bitmap;

public interface IBitmapDrawable {
    Bitmap getBitmap();
}
