package com.vc.image;

import android.graphics.Bitmap;
import android.widget.ImageView;

public interface Displayer {
    void loadCompletedisplay(ImageView imageView, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig);

    void loadCompletedisplay(ImageView imageView, Bitmap bitmap, BitmapDisplayConfig bitmapDisplayConfig, boolean z);

    void loadFailDisplay(ImageView imageView, Bitmap bitmap);
}
