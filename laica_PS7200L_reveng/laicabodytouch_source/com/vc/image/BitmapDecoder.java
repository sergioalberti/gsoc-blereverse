package com.vc.image;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.InputStream;

public class BitmapDecoder {
    private static final String TAG = "BitmapDecoder";

    private BitmapDecoder() {
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        try {
            return getBitmap(res, resId, options);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "decodeSampledBitmapFromResource内存溢出，如果频繁出现这个情况 可以尝试配置增加内存缓存大小");
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap decodeSampledBitmapFromFile(String filename, int reqWidth, int reqHeight) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        BitmapFactory.decodeFile(filename, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        try {
            return getBitmap(filename, options);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "decodeSampledBitmapFromFile内存溢出，如果频繁出现这个情况 可以尝试配置增加内存缓存大小");
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap decodeSampledBitmapFromDescriptor(FileDescriptor fileDescriptor) {
        try {
            return getBitmap(fileDescriptor, new Options());
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "decodeSampledBitmapFromDescriptor内存溢出，如果频繁出现这个情况 可以尝试配置增加内存缓存大小");
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap decodeSampledBitmapFromDescriptor(FileDescriptor fileDescriptor, int scale) {
        Bitmap bitmap = null;
        Options options = new Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, bitmap, options);
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        try {
            bitmap = getBitmap(fileDescriptor, options);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "decodeSampledBitmapFromDescriptor内存溢出，如果频繁出现这个情况 可以尝试配置增加内存缓存大小");
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap decodeSampledBitmapFromDescriptor(FileDescriptor fileDescriptor, int reqWidth, int reqHeight) {
        Bitmap bitmap = null;
        Options options = new Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, bitmap, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        try {
            bitmap = getBitmap(fileDescriptor, options);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "decodeSampledBitmapFromDescriptor内存溢出，如果频繁出现这个情况 可以尝试配置增加内存缓存大小");
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap decodeSampledBitmapFromInputStream(InputStream is) {
        try {
            return getBitmap(is, new Options());
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "decodeSampledBitmapFromDescriptor内存溢出，如果频繁出现这个情况 可以尝试配置增加内存缓存大小");
            e.printStackTrace();
            return null;
        }
    }

    public static int calculateInSampleSize(Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round(((float) height) / ((float) reqHeight));
            } else {
                inSampleSize = Math.round(((float) width) / ((float) reqWidth));
            }
            while (((float) (width * height)) / ((float) (inSampleSize * inSampleSize)) > ((float) ((reqWidth * reqHeight) * 2))) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private static Bitmap getBitmap(FileDescriptor fileDescriptor, Options options) {
        Bitmap bitmap = null;
        try {
            return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        } catch (OutOfMemoryError e) {
            options.inSampleSize++;
            if (options.inSampleSize <= 20) {
                return getBitmap(fileDescriptor, options);
            }
            return bitmap;
        } catch (Exception e2) {
            return bitmap;
        }
    }

    private static Bitmap getBitmap(String path, Options options) {
        Bitmap bitmap = null;
        try {
            return BitmapFactory.decodeFile(path, options);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "decodeSampledBitmapFromDescriptor内存溢出，如果频繁出现这个情况 可以尝试配置增加内存缓存大小");
            options.inSampleSize++;
            if (options.inSampleSize <= 20) {
                return getBitmap(path, options);
            }
            return bitmap;
        } catch (Exception e2) {
            return bitmap;
        }
    }

    private static Bitmap getBitmap(InputStream is, Options options) {
        Bitmap bitmap = null;
        try {
            return BitmapFactory.decodeStream(is, null, options);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "decodeSampledBitmapFromDescriptor内存溢出，如果频繁出现这个情况 可以尝试配置增加内存缓存大小");
            options.inSampleSize++;
            if (options.inSampleSize <= 20) {
                return getBitmap(is, options);
            }
            return bitmap;
        } catch (Exception e2) {
            Log.e("bitmapcache", e2.getMessage());
            return bitmap;
        }
    }

    private static Bitmap getBitmap(Resources res, int resId, Options options) {
        Bitmap bitmap = null;
        try {
            return BitmapFactory.decodeResource(res, resId, options);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "decodeSampledBitmapFromDescriptor内存溢出，如果频繁出现这个情况 可以尝试配置增加内存缓存大小");
            options.inSampleSize++;
            if (options.inSampleSize <= 20) {
                return getBitmap(res, resId, options);
            }
            return bitmap;
        } catch (Exception e2) {
            return bitmap;
        }
    }
}
