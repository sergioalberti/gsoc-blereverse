package p000m.framework.ui.widget.slidingmenu;

import android.graphics.Color;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.widget.HorizontalScrollView;

public class BodyContainer extends HorizontalScrollView {
    private static final int MENU_COVER_ALPHA = 230;
    private static final int MIN_FLING_VEL = 500;
    private float downX = 2.14748365E9f;
    private int maxVelocity;
    private SlidingMenu menu;
    private VelocityTracker tracker;

    public BodyContainer(SlidingMenu menu) {
        super(menu.getContext());
        this.menu = menu;
        this.maxVelocity = ViewConfiguration.get(menu.getContext()).getScaledMaximumFlingVelocity();
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case 0:
                this.downX = ev.getX();
                if (this.menu.isMenuShown() && this.downX > ((float) this.menu.getMenuWidth()) && ev.getY() > ((float) this.menu.getMenuConfig().titleHeight)) {
                    super.onInterceptTouchEvent(ev);
                    return true;
                }
            case 1:
            case 3:
                this.downX = 2.14748365E9f;
                break;
            case 2:
                if (!this.menu.isMenuShown() && this.downX > ((float) this.menu.getShowMenuWidth())) {
                    super.onInterceptTouchEvent(ev);
                    return false;
                }
        }
        return super.onInterceptTouchEvent(ev);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.tracker == null) {
            this.tracker = VelocityTracker.obtain();
        }
        this.tracker.addMovement(ev);
        switch (ev.getAction()) {
            case 1:
            case 3:
                if (this.menu.isMenuShown() && this.downX < ((float) this.menu.getMenuWidth())) {
                    return false;
                }
                this.downX = 2.14748365E9f;
                this.tracker.computeCurrentVelocity(1000, (float) this.maxVelocity);
                float velX = this.tracker.getXVelocity();
                this.tracker.recycle();
                this.tracker = null;
                int scrX = getScrollX();
                if (velX - 500.0f > 0.0f) {
                    this.menu.showMenu();
                } else if (velX + 500.0f < 0.0f) {
                    this.menu.hideMenu();
                } else if (scrX > this.menu.getMenuWidth() / 2) {
                    this.menu.hideMenu();
                } else {
                    this.menu.showMenu();
                }
                return true;
            default:
                if (!this.menu.isMenuShown() || this.downX >= ((float) this.menu.getMenuWidth())) {
                    return super.onTouchEvent(ev);
                }
                return false;
        }
    }

    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        this.menu.getMenuCover().setBackgroundColor(Color.argb((l * MENU_COVER_ALPHA) / this.menu.getMenuWidth(), 0, 0, 0));
    }
}
