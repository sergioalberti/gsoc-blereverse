package p000m.framework.ui.widget.pulltorefresh;

import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

public abstract class PullToRefreshListAdapter extends PullToRefreshBaseListAdapter {
    private ListInnerAdapter adapter;
    private boolean fling;
    private ScrollableListView listView = new ScrollableListView(getContext());
    private OnListStopScrollListener osListener;

    class C01901 implements OnScrollListener {
        private int firstVisibleItem;
        private int visibleItemCount;

        C01901() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            PullToRefreshListAdapter.this.fling = scrollState == 2;
            if (scrollState != 0) {
                return;
            }
            if (PullToRefreshListAdapter.this.osListener != null) {
                PullToRefreshListAdapter.this.osListener.onListStopScrolling(this.firstVisibleItem, this.visibleItemCount);
            } else if (PullToRefreshListAdapter.this.adapter != null) {
                PullToRefreshListAdapter.this.adapter.notifyDataSetChanged();
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            this.firstVisibleItem = firstVisibleItem;
            this.visibleItemCount = visibleItemCount;
            PullToRefreshListAdapter.this.onScroll(PullToRefreshListAdapter.this.listView, firstVisibleItem, visibleItemCount, totalItemCount);
        }
    }

    public PullToRefreshListAdapter(PullToRefreshView view) {
        super(view);
        this.listView.setOnScrollListener(new C01901());
        this.adapter = new ListInnerAdapter(this);
        this.listView.setAdapter(this.adapter);
    }

    public Scrollable getBodyView() {
        return this.listView;
    }

    public boolean isPullReady() {
        return this.listView.isReadyToPull();
    }

    public ListView getListView() {
        return this.listView;
    }

    public boolean isFling() {
        return this.fling;
    }

    public void onScroll(Scrollable scrollable, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.adapter.notifyDataSetChanged();
    }
}
