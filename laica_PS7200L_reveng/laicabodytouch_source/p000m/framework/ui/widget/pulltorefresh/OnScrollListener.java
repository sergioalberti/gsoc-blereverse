package p000m.framework.ui.widget.pulltorefresh;

public interface OnScrollListener {
    void onScrollChanged(Scrollable scrollable, int i, int i2, int i3, int i4);
}
