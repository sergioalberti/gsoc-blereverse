package p000m.framework.ui.widget.pulltorefresh;

public interface OnListStopScrollListener {
    void onListStopScrolling(int i, int i2);
}
