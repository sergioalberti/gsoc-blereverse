package p000m.framework.ui.widget.pulltorefresh;

import android.content.Context;
import android.view.View;

public abstract class PullToRefreshAdatper {
    private Context context;
    private PullToRefreshView parent;

    public abstract Scrollable getBodyView();

    public abstract View getHeaderView();

    public abstract boolean isPullReady();

    public abstract void onPullDown(int i);

    public abstract void onRequest();

    public PullToRefreshAdatper(PullToRefreshView view) {
        this.context = view.getContext();
        this.parent = view;
    }

    public Context getContext() {
        return this.context;
    }

    protected PullToRefreshView getParent() {
        return this.parent;
    }

    public void notifyDataSetChanged() {
        this.parent.stopPulling();
    }

    public void onReversed() {
    }
}
