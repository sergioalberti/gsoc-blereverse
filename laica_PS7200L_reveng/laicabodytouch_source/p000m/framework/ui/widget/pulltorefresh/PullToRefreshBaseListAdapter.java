package p000m.framework.ui.widget.pulltorefresh;

import android.view.View;
import android.view.ViewGroup;

public abstract class PullToRefreshBaseListAdapter extends PullToRefreshAdatper {
    public abstract int getCount();

    public abstract Object getItem(int i);

    public abstract long getItemId(int i);

    public abstract View getView(int i, View view, ViewGroup viewGroup);

    public abstract boolean isFling();

    public abstract void onScroll(Scrollable scrollable, int i, int i2, int i3);

    public PullToRefreshBaseListAdapter(PullToRefreshView view) {
        super(view);
    }
}
