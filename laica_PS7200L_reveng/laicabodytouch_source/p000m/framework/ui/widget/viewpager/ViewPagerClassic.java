package p000m.framework.ui.widget.viewpager;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import p000m.framework.utils.Utils;

public class ViewPagerClassic extends ViewGroup {
    private static final int SNAP_VELOCITY = 500;
    private static final int TOUCH_STATE_REST = 0;
    private static final int TOUCH_STATE_SCROLLING = 1;
    private ViewPagerAdapter adapter;
    private int currentScreen;
    private float lastMotionX;
    private float lastMotionY;
    private int mMaximumVelocity;
    private VelocityTracker mVelocityTracker;
    private Scroller scroller;
    private int touchSlop;
    private int touchState;

    class C02011 implements Interpolator {
        float[] values = new float[]{0.0f, 0.0157073f, 0.0314108f, 0.0471065f, 0.0627905f, 0.0784591f, 0.0941083f, 0.109734f, 0.125333f, 0.140901f, 0.156434f, 0.171929f, 0.187381f, 0.202787f, 0.218143f, 0.233445f, 0.24869f, 0.263873f, 0.278991f, 0.29404f, 0.309017f, 0.323917f, 0.338738f, 0.353475f, 0.368125f, 0.382683f, 0.397148f, 0.411514f, 0.425779f, 0.439939f, 0.45399f, 0.46793f, 0.481754f, 0.495459f, 0.509041f, 0.522499f, 0.535827f, 0.549023f, 0.562083f, 0.575005f, 0.587785f, 0.60042f, 0.612907f, 0.625243f, 0.637424f, 0.649448f, 0.661312f, 0.673013f, 0.684547f, 0.695913f, 0.707107f, 0.718126f, 0.728969f, 0.739631f, 0.750111f, 0.760406f, 0.770513f, 0.78043f, 0.790155f, 0.799685f, 0.809017f, 0.81815f, 0.827081f, 0.835807f, 0.844328f, 0.85264f, 0.860742f, 0.868632f, 0.876307f, 0.883766f, 0.891007f, 0.898028f, 0.904827f, 0.911403f, 0.917755f, 0.92388f, 0.929776f, 0.935444f, 0.940881f, 0.946085f, 0.951057f, 0.955793f, 0.960294f, 0.964557f, 0.968583f, 0.97237f, 0.975917f, 0.979223f, 0.982287f, 0.985109f, 0.987688f, 0.990024f, 0.992115f, 0.993961f, 0.995562f, 0.996917f, 0.998027f, 0.99889f, 0.999507f, 0.999877f, 1.0f};

        C02011() {
        }

        public float getInterpolation(float t) {
            return this.values[(int) (100.0f * t)];
        }
    }

    public ViewPagerClassic(Context context) {
        this(context, null);
    }

    public ViewPagerClassic(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ViewPagerClassic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.touchState = 0;
        init(context);
    }

    private void init(Context context) {
        this.scroller = new Scroller(getContext(), new C02011());
        ViewConfiguration configuration = ViewConfiguration.get(context);
        this.touchSlop = configuration.getScaledTouchSlop();
        this.mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
    }

    public int getCurrentScreen() {
        return this.currentScreen;
    }

    public void setCurrentScreen(int theCurrentScreen) {
        if (this.adapter != null) {
            if (!this.scroller.isFinished()) {
                this.scroller.abortAnimation();
            }
            int lastScreen = this.currentScreen;
            this.currentScreen = Math.max(0, Math.min(theCurrentScreen, getChildCount()));
            this.adapter.onScreenChange(this.currentScreen, lastScreen);
            int newX = this.currentScreen * Utils.getScreenWidth(getContext());
            this.scroller.startScroll(0, 0, newX, 0);
            scrollTo(newX, 0);
        }
    }

    public void computeScroll() {
        if (this.adapter != null) {
            if (this.scroller.computeScrollOffset()) {
                scrollTo(this.scroller.getCurrX(), this.scroller.getCurrY());
                postInvalidate();
                return;
            }
            int lastScreen = this.currentScreen;
            int scrX = this.scroller.getCurrX();
            int w = getWidth();
            int index = scrX / w;
            if (scrX % w > w / 2) {
                index++;
            }
            this.currentScreen = Math.max(0, Math.min(index, getChildCount() - 1));
            if (lastScreen != this.currentScreen && this.adapter != null) {
                this.adapter.onScreenChange(this.currentScreen, lastScreen);
            }
        }
    }

    public void setAdapter(ViewPagerAdapter adapter) {
        this.adapter = adapter;
        removeAllViews();
        this.currentScreen = 0;
        if (this.adapter != null) {
            int count = adapter.getCount();
            for (int i = 0; i < count; i++) {
                addView(adapter.getView(i, this));
            }
        }
    }

    protected void dispatchDraw(Canvas canvas) {
        if (this.adapter != null && getChildCount() > 0) {
            long drawingTime = getDrawingTime();
            if (this.currentScreen > 0) {
                drawChild(canvas, getChildAt(this.currentScreen - 1), drawingTime);
            }
            drawChild(canvas, getChildAt(this.currentScreen), drawingTime);
            if (this.currentScreen < getChildCount() - 1) {
                drawChild(canvas, getChildAt(this.currentScreen + 1), drawingTime);
            }
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.adapter == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }
        int i;
        int count = getChildCount();
        int maxHeight = 0;
        int adjustedWidthMeasureSpec = MeasureSpec.makeMeasureSpec(Utils.getScreenWidth(getContext()), 1073741824);
        for (i = 0; i < count; i++) {
            View child = getChildAt(i);
            child.measure(adjustedWidthMeasureSpec, 0);
            int height = child.getMeasuredHeight();
            if (height > maxHeight) {
                maxHeight = height;
            }
        }
        int adjustedHeightMeasureSpec = MeasureSpec.makeMeasureSpec(maxHeight, 1073741824);
        super.onMeasure(adjustedWidthMeasureSpec, adjustedHeightMeasureSpec);
        for (i = 0; i < count; i++) {
            getChildAt(i).measure(adjustedWidthMeasureSpec, adjustedHeightMeasureSpec);
        }
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (this.adapter != null) {
            int cLeft = 0;
            int cWidth = right - left;
            int cHeight = bottom - top;
            int count = getChildCount();
            for (int i = 0; i < count; i++) {
                View child = getChildAt(i);
                if (child.getVisibility() != 8) {
                    child.layout(cLeft, 0, cLeft + cWidth, cHeight);
                    cLeft += cWidth;
                }
            }
        }
    }

    public boolean dispatchUnhandledMove(View focused, int direction) {
        if (this.adapter == null) {
            return super.dispatchUnhandledMove(focused, direction);
        }
        if (direction == 17) {
            if (this.currentScreen > 0) {
                scrollToScreen(this.currentScreen - 1);
                return true;
            }
        } else if (direction == 66 && this.currentScreen < getChildCount() - 1) {
            scrollToScreen(this.currentScreen + 1);
            return true;
        }
        return super.dispatchUnhandledMove(focused, direction);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        if (action == 2 && this.touchState != 0) {
            return true;
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(ev);
        switch (action) {
            case 0:
                float x1 = ev.getX();
                float y1 = ev.getY();
                this.lastMotionX = x1;
                this.lastMotionY = y1;
                this.touchState = this.scroller.isFinished() ? 0 : 1;
                break;
            case 1:
            case 3:
                if (this.mVelocityTracker != null) {
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = null;
                }
                this.touchState = 0;
                break;
            case 2:
                handleInterceptMove(ev);
                break;
        }
        if (this.touchState == 0) {
            return false;
        }
        return true;
    }

    private void handleInterceptMove(MotionEvent ev) {
        boolean xMoved;
        float x = ev.getX();
        int yDiff = (int) Math.abs(ev.getY() - this.lastMotionY);
        if (((int) Math.abs(x - this.lastMotionX)) > this.touchSlop) {
            xMoved = true;
        } else {
            xMoved = false;
        }
        boolean yMoved;
        if (yDiff > this.touchSlop) {
            yMoved = true;
        } else {
            yMoved = false;
        }
        if ((xMoved || yMoved) && xMoved) {
            this.touchState = 1;
            this.lastMotionX = x;
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.adapter == null) {
            return false;
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(ev);
        int action = ev.getAction();
        float x = ev.getX();
        switch (action) {
            case 0:
                if (this.touchState != 0) {
                    if (!this.scroller.isFinished()) {
                        this.scroller.abortAnimation();
                    }
                    this.lastMotionX = x;
                    break;
                }
                break;
            case 1:
                if (this.touchState == 1) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                    int velocityX = (int) velocityTracker.getXVelocity();
                    if (velocityX > SNAP_VELOCITY && this.currentScreen > 0) {
                        scrollToScreen(this.currentScreen - 1);
                    } else if (velocityX >= -500 || this.currentScreen >= getChildCount() - 1) {
                        int screenWidth = getWidth();
                        scrollToScreen((getScrollX() + (screenWidth / 2)) / screenWidth);
                    } else {
                        scrollToScreen(this.currentScreen + 1);
                    }
                    if (this.mVelocityTracker != null) {
                        this.mVelocityTracker.recycle();
                        this.mVelocityTracker = null;
                    }
                }
                this.touchState = 0;
                break;
            case 2:
                if (this.touchState != 1) {
                    if (onInterceptTouchEvent(ev) && this.touchState == 1) {
                        handleScrollMove(ev);
                        break;
                    }
                }
                handleScrollMove(ev);
                break;
            case 3:
                this.touchState = 0;
                break;
        }
        return true;
    }

    private void handleScrollMove(MotionEvent ev) {
        if (this.adapter != null) {
            float x1 = ev.getX();
            int deltaX = (int) (this.lastMotionX - x1);
            this.lastMotionX = x1;
            if (deltaX < 0) {
                if (getScrollX() > 0) {
                    scrollBy(Math.max(-getScrollX(), deltaX), 0);
                }
            } else if (deltaX > 0 && getChildCount() != 0) {
                int availableToScroll = (getChildAt(getChildCount() - 1).getRight() - getScrollX()) - getWidth();
                if (availableToScroll > 0) {
                    scrollBy(Math.min(availableToScroll, deltaX), 0);
                }
            }
        }
    }

    public void scrollToScreen(int whichScreen) {
        scrollToScreen(whichScreen, false);
    }

    private void scrollToScreen(int whichScreen, boolean immediate) {
        boolean changingScreens;
        if (whichScreen != this.currentScreen) {
            changingScreens = true;
        } else {
            changingScreens = false;
        }
        View focusedChild = getFocusedChild();
        if (focusedChild != null && changingScreens && focusedChild == getChildAt(this.currentScreen)) {
            focusedChild.clearFocus();
        }
        int delta = (whichScreen * getWidth()) - getScrollX();
        this.scroller.startScroll(getScrollX(), 0, delta, 0, immediate ? 0 : Math.abs(delta) / 2);
        invalidate();
    }

    public void scrollLeft() {
        if (this.adapter != null && this.currentScreen > 0 && this.scroller.isFinished()) {
            scrollToScreen(this.currentScreen - 1);
        }
    }

    public void scrollRight() {
        if (this.adapter != null && this.currentScreen < getChildCount() - 1 && this.scroller.isFinished()) {
            scrollToScreen(this.currentScreen + 1);
        }
    }
}
