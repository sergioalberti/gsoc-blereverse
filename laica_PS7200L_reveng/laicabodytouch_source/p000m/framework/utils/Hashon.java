package p000m.framework.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Hashon {
    public HashMap<String, Object> fromJson(String jsonStr) {
        try {
            if (jsonStr.startsWith("[") && jsonStr.endsWith("]")) {
                jsonStr = "{\"fakelist\":" + jsonStr + "}";
            }
            return fromJson(new JSONObject(jsonStr));
        } catch (Throwable t) {
            t.printStackTrace();
            return new HashMap();
        }
    }

    private HashMap<String, Object> fromJson(JSONObject json) throws JSONException {
        HashMap<String, Object> map = new HashMap();
        Iterator<String> iKey = json.keys();
        while (iKey.hasNext()) {
            String key = (String) iKey.next();
            Object value = json.opt(key);
            if (JSONObject.NULL.equals(value)) {
                value = null;
            }
            if (value != null) {
                if (value instanceof JSONObject) {
                    value = fromJson((JSONObject) value);
                } else if (value instanceof JSONArray) {
                    value = fromJson((JSONArray) value);
                }
                map.put(key, value);
            }
        }
        return map;
    }

    private ArrayList<Object> fromJson(JSONArray array) throws JSONException {
        ArrayList<Object> list = new ArrayList();
        int size = array.length();
        for (int i = 0; i < size; i++) {
            Object value = array.opt(i);
            if (value instanceof JSONObject) {
                value = fromJson((JSONObject) value);
            } else if (value instanceof JSONArray) {
                value = fromJson((JSONArray) value);
            }
            list.add(value);
        }
        return list;
    }

    public String fromHashMap(HashMap<String, Object> map) {
        try {
            return getJSONObject(map).toString();
        } catch (Throwable t) {
            t.printStackTrace();
            return "";
        }
    }

    private JSONObject getJSONObject(HashMap<String, Object> map) throws JSONException {
        JSONObject json = new JSONObject();
        for (Entry<String, Object> entry : map.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof HashMap) {
                value = getJSONObject((HashMap) value);
            } else if (value instanceof ArrayList) {
                value = getJSONArray((ArrayList) value);
            } else if (isBasicArray(value)) {
                value = getJSONArray(arrayToList(value));
            }
            json.put((String) entry.getKey(), value);
        }
        return json;
    }

    private boolean isBasicArray(Object value) {
        return (value instanceof byte[]) || (value instanceof short[]) || (value instanceof int[]) || (value instanceof long[]) || (value instanceof float[]) || (value instanceof double[]) || (value instanceof char[]) || (value instanceof boolean[]) || (value instanceof String[]);
    }

    private ArrayList<?> arrayToList(Object value) {
        int i = 0;
        int length;
        if (value instanceof byte[]) {
            ArrayList<?> list = new ArrayList();
            byte[] bArr = (byte[]) value;
            length = bArr.length;
            while (i < length) {
                list.add(Byte.valueOf(bArr[i]));
                i++;
            }
            return list;
        } else if (value instanceof short[]) {
            ArrayList<Short> list2 = new ArrayList();
            short[] sArr = (short[]) value;
            length = sArr.length;
            while (i < length) {
                list2.add(Short.valueOf(sArr[i]));
                i++;
            }
            return list2;
        } else if (value instanceof int[]) {
            ArrayList<Integer> list3 = new ArrayList();
            int[] iArr = (int[]) value;
            length = iArr.length;
            while (i < length) {
                list3.add(Integer.valueOf(iArr[i]));
                i++;
            }
            return list3;
        } else if (value instanceof long[]) {
            ArrayList<Long> list4 = new ArrayList();
            long[] jArr = (long[]) value;
            length = jArr.length;
            while (i < length) {
                list4.add(Long.valueOf(jArr[i]));
                i++;
            }
            return list4;
        } else if (value instanceof float[]) {
            ArrayList<Float> list5 = new ArrayList();
            float[] fArr = (float[]) value;
            length = fArr.length;
            while (i < length) {
                list5.add(Float.valueOf(fArr[i]));
                i++;
            }
            return list5;
        } else if (value instanceof double[]) {
            ArrayList<Double> list6 = new ArrayList();
            double[] dArr = (double[]) value;
            length = dArr.length;
            while (i < length) {
                list6.add(Double.valueOf(dArr[i]));
                i++;
            }
            return list6;
        } else if (value instanceof char[]) {
            ArrayList<Character> list7 = new ArrayList();
            char[] cArr = (char[]) value;
            length = cArr.length;
            while (i < length) {
                list7.add(Character.valueOf(cArr[i]));
                i++;
            }
            return list7;
        } else if (value instanceof boolean[]) {
            ArrayList<Boolean> list8 = new ArrayList();
            boolean[] zArr = (boolean[]) value;
            length = zArr.length;
            while (i < length) {
                list8.add(Boolean.valueOf(zArr[i]));
                i++;
            }
            return list8;
        } else if (!(value instanceof String[])) {
            return null;
        } else {
            ArrayList<String> list9 = new ArrayList();
            String[] strArr = (String[]) value;
            length = strArr.length;
            while (i < length) {
                list9.add(strArr[i]);
                i++;
            }
            return list9;
        }
    }

    private JSONArray getJSONArray(ArrayList<Object> list) throws JSONException {
        JSONArray array = new JSONArray();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Object value = it.next();
            if (value instanceof HashMap) {
                value = getJSONObject((HashMap) value);
            } else if (value instanceof ArrayList) {
                value = getJSONArray((ArrayList) value);
            }
            array.put(value);
        }
        return array;
    }

    public String format(String jsonStr) {
        try {
            return format("", fromJson(jsonStr));
        } catch (Throwable t) {
            t.printStackTrace();
            return "";
        }
    }

    private String format(String sepStr, HashMap<String, Object> map) {
        StringBuffer sb = new StringBuffer();
        sb.append("{\n");
        String mySepStr = new StringBuilder(String.valueOf(sepStr)).append("\t").toString();
        int i = 0;
        for (Entry<String, Object> entry : map.entrySet()) {
            if (i > 0) {
                sb.append(",\n");
            }
            sb.append(mySepStr).append('\"').append((String) entry.getKey()).append("\":");
            Object value = entry.getValue();
            if (value instanceof HashMap) {
                sb.append(format(mySepStr, (HashMap) value));
            } else if (value instanceof ArrayList) {
                sb.append(format(mySepStr, (ArrayList) value));
            } else if (value instanceof String) {
                sb.append('\"').append(value).append('\"');
            } else {
                sb.append(value);
            }
            i++;
        }
        sb.append('\n').append(sepStr).append('}');
        return sb.toString();
    }

    private String format(String sepStr, ArrayList<Object> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("[\n");
        String mySepStr = new StringBuilder(String.valueOf(sepStr)).append("\t").toString();
        int i = 0;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Object value = it.next();
            if (i > 0) {
                sb.append(",\n");
            }
            sb.append(mySepStr);
            if (value instanceof HashMap) {
                sb.append(format(mySepStr, (HashMap) value));
            } else if (value instanceof ArrayList) {
                sb.append(format(mySepStr, (ArrayList) value));
            } else if (value instanceof String) {
                sb.append('\"').append(value).append('\"');
            } else {
                sb.append(value);
            }
            i++;
        }
        sb.append('\n').append(sepStr).append(']');
        return sb.toString();
    }
}
