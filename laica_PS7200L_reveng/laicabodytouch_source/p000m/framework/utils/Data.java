package p000m.framework.utils;

import com.vc.cloudbalance.common.WeightUnitHelper;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.HashMap;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Data {
    private static final String CHAT_SET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static Hashon hashon = new Hashon();

    public static byte[] SHA1(String text) throws Throwable {
        byte[] data = text.getBytes("utf-8");
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(data);
        return md.digest();
    }

    public static byte[] AES128Encode(String key, String text) throws Throwable {
        byte[] keyBytes = key.getBytes("UTF-8");
        byte[] keyBytes16 = new byte[16];
        System.arraycopy(keyBytes, 0, keyBytes16, 0, Math.min(keyBytes.length, 16));
        byte[] data = text.getBytes("UTF-8");
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes16, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
        cipher.init(1, keySpec);
        byte[] cipherText = new byte[cipher.getOutputSize(data.length)];
        cipher.doFinal(cipherText, cipher.update(data, 0, data.length, cipherText, 0));
        return cipherText;
    }

    public static byte[] AES128Encode(byte[] key, String text) throws Throwable {
        byte[] data = text.getBytes("UTF-8");
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
        cipher.init(1, keySpec);
        byte[] cipherText = new byte[cipher.getOutputSize(data.length)];
        cipher.doFinal(cipherText, cipher.update(data, 0, data.length, cipherText, 0));
        return cipherText;
    }

    public static String AES128Decode(String key, byte[] cipherText) throws Throwable {
        return new String(Data.AES128Decode(key.getBytes("UTF-8"), cipherText), "UTF-8");
    }

    public static byte[] AES128Decode(byte[] keyBytes, byte[] cipherText) throws Throwable {
        byte[] keyBytes16 = new byte[16];
        System.arraycopy(keyBytes, 0, keyBytes16, 0, Math.min(keyBytes.length, 16));
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes16, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding", "BC");
        cipher.init(2, keySpec);
        byte[] plainText = new byte[cipher.getOutputSize(cipherText.length)];
        int ptLength = cipher.update(cipherText, 0, cipherText.length, plainText, 0);
        ptLength += cipher.doFinal(plainText, ptLength);
        return plainText;
    }

    public static String byteToHex(byte[] data) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            buffer.append(String.format("%02x", new Object[]{Byte.valueOf(data[i])}));
        }
        return buffer.toString();
    }

    public static HashMap<String, Object> parseJson(String json) {
        return hashon.fromJson(json);
    }

    public static String parseJson(HashMap<String, Object> map) {
        return hashon.fromHashMap(map);
    }

    public static String base62(long value) {
        String result = value == 0 ? WeightUnitHelper.Kg : "";
        while (value > 0) {
            int v = (int) (value % 62);
            value /= 62;
            result = new StringBuilder(String.valueOf(CHAT_SET.charAt(v))).append(result).toString();
        }
        return result;
    }

    public static String MD5(String data) {
        if (data == null) {
            return null;
        }
        byte[] tmp = Data.rawMD5(data);
        if (tmp != null) {
            return HEX.toHex(tmp);
        }
        return null;
    }

    public static byte[] rawMD5(String data) {
        if (data == null) {
            return null;
        }
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(data.getBytes("utf-8"));
            return md.digest();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String urlEncode(String s, String enc) throws Throwable {
        return URLEncoder.encode(s, enc).replace("\\+", "%20");
    }
}
