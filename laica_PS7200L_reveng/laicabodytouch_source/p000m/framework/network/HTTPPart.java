package p000m.framework.network;

import java.io.InputStream;
import org.apache.http.entity.InputStreamEntity;

public abstract class HTTPPart {
    protected abstract InputStream getInputStream() throws Throwable;

    protected abstract long length() throws Throwable;

    public InputStreamEntity getInputStreamEntity() throws Throwable {
        return new InputStreamEntity(getInputStream(), length());
    }
}
