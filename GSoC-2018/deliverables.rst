Deliverables and time schedule
==============================
During the Community Bonding period (April 23 - May 14) the deliverables presented in the initial proposal have been modified.
In agreement with the mentors it was decided to spend more time writing the reverse-engineering guide, which probably turns out to be the most useful part.

As a result, the timeline has been adapted as follows:

* 14/05 - 08/07 (8 weeks)
    This weeks will be focused on writing the guide. The goal is to produce a document that deals with the basic principles of BLE, the sniffing of Bluetooth packages via Android,
    the use of Wireshark to see their content, the decompilation of Android applications and the use of the BlueZ stack to communicate with BLE devices.

    The point of reference is `this thesis <http://sl-lab.it/dokuwiki/lib/exe/fetch.php/tesi:tesialbertitermovalvole.pdf>`_, which will be translated and made as general as possible.
    The idea is to identify which practices can be common to all devices and which are applicable only to the radiator valves described in the thesis.
    For this reason, I will try to apply the same procedures to other types of valves or other BLE devices.

    The last part of this period (*probably a week*) will be dedicated to the construction of a mechanical device (using a spring) that allows to setup and test the valves in the absence of a radiator.
    This activity will also be included in the guide and documented.

* 09/07 - 06/08 (4 weeks)
    Recently a Python library that allows the use of these valves `has been integrated <https://www.home-assistant.io/components/climate.eq3btsmart>`_ into *home-assistant*.
    This period will be used to analyze it, to understand if it can be improved and if it can be used as a "stand alone" library.

    Depending on this, the end of the activity will consist in documenting and packaging the Python library or the `already developed one <http://sl-lab.it/dokuwiki/doku.php/tesi:reveng-termovalvole>`_ (which must also be documented).
