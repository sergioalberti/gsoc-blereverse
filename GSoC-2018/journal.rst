Journal (weekly reports)
========================

GSoC Week 1
-----------

(https://lists.debian.org/debian-outreach/2018/05/msg00016.html)

    * Started writing the reverse engineering guide for BLE devices, translating parts from the original
      thesis and making it more complete and intuitive.

GSoC Week 2
-----------

(https://lists.debian.org/debian-outreach/2018/05/msg00032.html)

    * continued to write the reverse-engineering guide for BLE devices focusing on providing alternative
      methods to achieve the results and expanding as much as possible what is already there (to make it clearer)

    * purchased a new BLE device (on which to test these practices) in order to expand the examples
      and identify which techniques are applicable to each device and which only to the radiator valves.

GSoC Week 3
-----------

(https://lists.debian.org/debian-outreach/2018/06/msg00008.html)

    * translated all the guide' sections that formally document the protocol used by the radiator valves and
      started working on the chapter that will contain information about the BlueZ stack and examples on creating scripts that use it

    * acquired and started to inspect (using Wireshark) the log files produced by the Bluetooth sniff of a BLE scale (Laica PS7200L)

GSoC Week 4
-----------

(https://lists.debian.org/debian-outreach/2018/06/msg00033.html)

    * corrected and integrated the first two chapters of the guide according to the advice of my mentor

    * continued to write the chapters 3 and 4, which now cover all the details related to the radiator valve's protocol

    * identified some important information about the Laica PS7200L protocol, which, however, are not enough to understand it completely (see protocol_info.txt)

GSoC Week 5
-----------

(https://lists.debian.org/debian-outreach/2018/06/msg00081.html)

    * searched and tested software in these areas: free Class Diagram generators, BLE support in Android emulators, ".so" libraries (ARM targeted) decompilers

    * integrated the second chapter with examples/ideas and alternatives to proprietary software (according to what proposed by my mentor)

    * managed to decompile the proprietary library "libyohealth.so" both in ARM assembly (using objdump and an ARM toolchain) and in C code (using RetDec decompiler)

GSoC Week 6
-----------

(https://lists.debian.org/debian-outreach/2018/06/msg00108.html)

    * translated and uploaded a "reference implementation" of the scripts

    * added section concerning the differences between BR/EDR and BLE

    * added section concerning hcitool and gatttool (part of the Bluez Stack)

    * "created" and documented a device to setup the valves without a radiator

GSoC Week 7
-----------

(https://lists.debian.org/debian-outreach/2018/06/msg00140.html)

    * managed to get some working script for the Laica PS7200L BLE scale. (not totally reverse-engineered, but weight reading works the right way)

    * added sections related to the Bluetooth Advertising packages and the tools needed to read their content (hcidump + hcitool)

    * small changes to the structure of the sections, so that (in the future) anyone can add the work done on other BLE devices

GSoC Week 8
-----------

(https://lists.debian.org/debian-outreach/2018/07/msg00028.html)

    * integrated all scripts with controls, such as: Bluetooth status check, MAC address validation..

    * added a "Contributions" page in the form of meta-guide on how to add BLE devices to the project

    * concluded the parts of the guide concerning the BLE scale

    * added missing references/images to the examples regarding the radiator valves

    * corrections/additions (as suggested by my mentor)

GSoC Week 9
-----------

(https://lists.debian.org/debian-outreach/2018/07/msg00052.html)

    * created a first version of the eq3eqiva Deb package

GSoC Week 10
------------

(https://lists.debian.org/debian-outreach/2018/07/msg00074.html)

    * basically concluded the creation of the Debian eq3eqiva package (add scripts, made it conform to Lintian, ecc..)

    * added option to enable the parsing of BLE notification and some bug fix

    * brief analysis of the "python-eq3bt" library

GSoC Week 11
------------

(https://lists.debian.org/debian-outreach/2018/07/msg00103.html)

    * reveng-guide: fixed style problems (css) and completed some parts

    * reveng-guide: update "Script Creation" section to make it agree with the new implementation of the scripts

    * script: added exit codes, some checks and bugfixes

    * script: created unit tests for eq3eqiva

GSoC Week 12
------------

(https://lists.debian.org/debian-outreach/2018/08/msg00008.html)

    * concluded the unit tests (for both EQ3 Eqiva and Laica scripts)

    * fixed small bugs identified by the tests

    * updated the eq3eqiva deb package in order to match the "updated_implementation"

    * modified the reverse engineering guide structure (at file/directory level) to make it easier to update and maintain
