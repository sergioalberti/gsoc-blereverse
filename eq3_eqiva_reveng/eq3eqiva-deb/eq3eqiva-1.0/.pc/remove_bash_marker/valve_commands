#!/bin/bash

. /usr/share/eq3eqiva/basic_functions

#sends the current date and time to the valve.
#generally useful as first command to send to the valve to receive information on its status
#input: <device address>
#output: notification received after the execution of the command
send_init(){
   #composition of 03YYMMDDhhmmss
   COMMAND=$(printf "%02x" 3)
   year=$(( $(date +"%Y") % 100 ))
   year=$(printf "%02x" 0x$(echo "obase=16 ; $year" | bc))

   tmp=$(date +"%m")
   month=$(printf "%02x" 0x$(echo "obase=16 ; $tmp" | bc))

   tmp=$(date +"%d")
   day=$(printf "%02x" 0x$(echo "obase=16 ; $tmp" | bc))

   tmp=$(date +"%H")
   hour=$(printf "%02x" 0x$(echo "obase=16 ; $tmp" | bc))

   tmp=$(date +"%M")
   minute=$(printf "%02x" 0x$(echo "obase=16 ; $tmp" | bc))

   tmp=$(date +"%S")
   seconds=$(printf "%02x" 0x$(echo "obase=16 ; $tmp" | bc))

   COMMAND=$COMMAND$year$month$day$hour$minute$seconds
   echo $( send_command $1 $COMMAND )
}

#activates boost mode
#input: <device address>
#output: notification received after the execution of the command
boost_mode() {
   echo $( send_command $1 4501 )
}

#disables boost mode
#input: <device address>
#output: notification received after the execution of the command
stop_boost_mode() {
   echo $( send_command $1 4500 )
}

#activates auto mode
#input: <device address>
#output: notification received after the execution of the command
auto_mode() {
   echo $( send_command $1 4000 )
}

#activates manual mode
#input: <device address>
#output: notification received after the execution of the command
manual_mode() {
   echo $( send_command $1 4040 )
}

#changes the temperature
#input: <device address> <temperature>
#output: notification received after the execution of the command
set_temperature() {
   temp=$( calculate_temp $2 )
   echo $( send_command $1 41$temp )
}

#changes the values of the comfort and reduced temperature
#input: <device address> <comfort temp> <reduct temp>
#output: notification received after the execution of the command
set_comfort_reduction_temp() {
   comfort=$( calculate_temp $2 )
   reduction=$( calculate_temp $3 )
   echo $( send_command $1 11$comfort$reduction )
}

#activates the holiday mode until the term defined by the parameters
#
#input: <device addr> <DD/MM/YYYY> <hh:mm> <temperature>
#output: notification containing the status of the valve and details on the end of the holiday
holiday_mode() {
   data_temp=$( calculate_temp_128 $4 )

   tmp=$(echo $2 | cut -d "/" -f 1)
   day=$(printf "%02x" 0x$(echo "obase=16 ; $tmp" | bc))

   tmp=$(echo $2 | cut -d "/" -f 2)
   month=$(printf "%02x" 0x$(echo "obase=16 ; $tmp" | bc))

   tmp=$(echo $2 | cut -d "/" -f 3)
   year=$(printf "%02x" 0x$(echo "obase=16 ; ($tmp%100) " | bc))

   hour=$(echo $3 | cut -d ":" -f 1)
   hour=$(( $hour % 24 ))
   min=$(echo $3 | cut -d ":" -f 2)

   if [ $min -lt 16 ] || [ $min -gt 44 ] ; then
      MINUTE_FACTOR=0
      if [ $min -gt 44 ] ; then
         hour=$(( ($hour+1)%24 )) #if greater than XX:44 go to the next hour
      fi
   else
      MINUTE_FACTOR=1
   fi

   hour_coded=$(printf "%02x" 0x$(echo "obase=16 ; ($hour*2 + $MINUTE_FACTOR)" | bc))
   echo $( send_command $1 40$data_temp$day$year$hour_coded$month )
}

#requires a daily schedule
#days are numbered from 00 (Saturday) to 06 (Friday)
#input: <device address> <day>
#output: notification containing the requested schedule
read_profile() {
   if [ $2 -lt 0 -o $2 -gt 6 ]; then
      echo "Week goes from 00 (saturday) to 06 (friday)."
      return
   fi

   day=$(printf "%02x" $2)
   echo $( send_command $1 20$day )
}

#sets a daily schedule
#days are numbered from 00 (Saturday) to 06 (Friday)
#input: <device address> <day> <int1> [int2] [int3] [int4] [int5] [int6] [int7]
#   each interval must be in the form TEMP/HH:MM.
#   in the last specified interval the time must be 24:00 or 00:00
#   in order to have represented the schedule of the whole day
#output:
#   notification that states the change has occurred
set_profile() {
   if [ $2 -lt 0 -o $2 -gt 6 ]; then
      echo "Week goes from 00 (saturday) to 06 (friday)."
      return
   fi

   COMMAND="10"$(printf "%02x" $2)
   actual_param=3

   while [ -n "${!actual_param}" ] && [ $actual_param -lt 10 ];
   do
      interval=${!actual_param}
      temp=$( calculate_temp $(echo $interval | cut -d "/" -f 1))
      end_time=$(echo $interval | cut -d "/" -f 2)

      hour=$(( 10#$( echo $end_time | cut -d ":" -f 1 ) ))
      minutes=$(( 10#$( echo $end_time | cut -d ":" -f 2 ) ))

      end_time=$(( $hour * 60 + $minutes ))
      end_time=$(( $end_time / 10 ))
      end_time=$(echo "obase=16 ; $end_time" | bc)

      COMMAND=$COMMAND$temp$end_time
      actual_param=$(( $actual_param + 1 ))
   done

   echo $( send_command $1 $COMMAND )
}

#locks the physical keys on the valve
#input: <device address>
#output: notification received after the execution of the command
lock() {
   echo $( send_command $1 8001 )
}

#unlocks the physical keys on the valve
#input: <device address>
#output: notification received after the execution of the command
unlock() {
   echo $( send_command $1 8000 )
}

#sets temperature and duration of the window mode
#input: <device address> <temperature> <duration>
#output: notification received after the execution of the command
set_window() {
   temp=$( calculate_temp $2 )

   if [ $3 -lt 0 -o $3 -gt 60 ]; then
      echo "Duration must be between 0 and 60."
      return
   fi

   #round up to a multiple of 5 and divide by 5
   min=$(( ($3+2)/5*5 ))
   min=$(printf "%02x" 0x$(echo "scale=0 ; obase=16 ; $min/5" | bc ))

   echo $( send_command $1 14$temp$min )
}

#sets the offset temperature. (between -3.5 and +3.5)
#input: <device address> <temperature>
#output: notification received after the execution of the command
set_offset() {
  check=$( echo "$2 < -3.5 || $2 > 3.5" | bc )
  if [ $check -eq 1 ]; then
     echo "Temperature must be between -3.5 and 3.5."
     return
  fi

  temp=$(awk -v req="$2" 'BEGIN { rounded = sprintf("%.0f", req*2); print rounded }')
  temp=$(echo "scale=1; $temp/2" | bc)
  temp=$(printf "%02x" 0x$(echo "scale=0; obase=16 ; (($temp *2 ) + 7.0)/1" | bc))

  echo $( send_command $1 13$temp )
}
