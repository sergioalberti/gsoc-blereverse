#!/bin/bash

#USAGE:
#	./manual_mode.sh <valve_name> [valve_name ...]
#	puts one or more valves in manual mode.
#	valve's names are supplied as a command line argument.

. ./valve_commands.sh

if [ -z $1 ]; then
	printf "Usage: ./manual_mode.sh <valve_name> [valve_name ...]\n"
	exit
fi

for name in "$@"; do
	address=$( search_by_name $name $VALVE_FILE )
	if [ "$address" != "-1" ]; then
		manual_mode $address
	else
		printf "%s valve not found\n" "$name"
	fi
done
