#!/bin/bash

. ./config.sh

#input: temperature in decimal base.
#output: (temperature*2) in hexadecimal base, rounded to XX.0 or XX.5
calculate_temp() {
   temp=$(awk -v req="$1" 'BEGIN { rounded = sprintf("%.0f", req*2); print rounded }')
   temp=$(printf "%02x" 0x$(echo "scale=0; obase=16 ; $temp/1" | bc))

   echo $temp
}

#input: temperature in decimal base.
#output: (temperature*2)+128 in hexadecimal base, rounded to XX.0 or XX.5
calculate_temp_128() {
   temp=$(awk -v req="$1" 'BEGIN { rounded = sprintf("%.0f", (req*2)+128); print rounded }')
   temp=$(echo "obase=16; $temp" | bc)

   echo $temp
}

#sends a command to the valve
#input: <device_address> <command>
#output: value received after the execution of the command
send_command() {
   output=$(timeout $TIMEOUT_SEC gatttool -b $1 --char-write-req -a 0x0411 -n $2 --listen)
   ret_value=${output#*: }  #removes the part of the string that precedes ":"
   echo $ret_value
}

#input: bytes 7, 8, 9, 10 of a notification
#output: prints date and time of the end of the holiday (valve in holiday mode)
parse_holiday_params() {
   day=$(echo $1 | tr 'a-z' 'A-Z')
   day=$(echo "ibase=16 ; scale=0 ; $day" | bc)
   year=$(echo $2 | tr 'a-z' 'A-Z')
   year=$(echo "ibase=16 ; scale=0 ; $year" | bc)
   month=$(echo $4 | tr 'a-z' 'A-Z')
   month=$(echo "ibase=16 ; scale=0 ; $month" | bc)
   hour=$(echo $3 | tr 'a-z' 'A-Z')
   hour=$(echo "ibase=16 ; scale=0 ; $hour" | bc)

   #the last bit states if minutes are 00 or 30
   last_bit=$(echo "obase=2 ; $hour" | bc)
   length=$((${#last_bit}-1))
   last_bit=`echo ${last_bit:$length:1}`

   minutes=00
   if [ $last_bit -eq 1 ]
   then
      hour=$(( $hour - 1 ))
      minutes=30
   fi

   hour=$(echo "scale=0;$hour/2" | bc)
   printf "holiday ends: %s at %s\n" "$day/$month/$year" "$hour:$minutes"
}

#input:
#   content of the notification received from the valve
#   note: each byte is represented by a different parameter
#output:
#   translates the notification into "readable" content
parse_return_value() {
   #$1 = 0x21 = 33 -> received daily schedule
   #$1 = 0x02 = 02 -> received another type of notification
   notify=$(echo "ibase=16 ; $1" | bc )

   if [ $notify -eq 33 ]; then
      temp_param=3
      end_time_param=4
      last_interval=0

      while [ $end_time_param -lt 17 -a $last_interval -ne 1 ];
      do
         temperature=$(echo ${!temp_param} | tr 'a-z' 'A-Z')
         end_time=$(echo ${!end_time_param} | tr 'a-z' 'A-Z')
         temperature=$(echo "ibase=16 ; scale=1 ; $temperature/2 " | bc)
         end_time=$(echo "ibase=16 ; $end_time" | bc)
         end_time=$(( $end_time * 10 ))

         r=$(( $end_time % 60 ))
         end_time=$(( $end_time/60 ))
         printf "%s degrees until %02d:%02d\n" "$temperature" "$end_time" "$r"

         if [ $end_time == "24" -o $end_time == "00" ]; then
            last_interval=1
         else
            temp_param=$(( $temp_param + 2 ))
            end_time_param=$(( $end_time_param + 2 ))
         fi
      done
   elif [ $notify -eq 2 ]; then
      #$2=01 -> information about valve' status
      #$2=02 -> successful schedule change
      if [ $2 -eq 1 ]; then
            #check the third byte of the return value
            active_mode=$(echo $3 | tr 'a-z' 'A-Z')
            first_bit=$(echo ${active_mode:0:1})
            active_mode=$(echo "ibase=16 ; $active_mode" | bc)

            if [ $first_bit -eq 2 ] ; then
               #command block on ($active_mode=28/29/2A/2C/2D/2E)
               #subtract 32 (0x20) to go back to the prevous cases (08/09/0A etc...)
               printf "//LOCKED!//\n"
               active_mode=$(( $active_mode - 32 ))
            fi

            holiday=0
            case "$active_mode" in
               8) printf "AUTO MODE\n";;                #auto mode 08
               9) printf "MANUAL MODE\n";;              #manual mode 09
               10)   printf "HOLIDAY MODE\n"            #vacation mode 0A
                     holiday=1;;
               12)   printf "BOOST (auto mode)\n";;     #boost mode 0C -> end boost go to auto
               13)   printf "BOOST (manual mode)\n";;   #boost mode 0D -> end boost go to man
               14)   printf "BOOST (holiday mode)\n"    #boost mode 0E -> end boost go to holiday
                     holiday=1;;
            esac

            #sixth byte of the ret value contains (temperature*2)
            double_temp=$(echo $6 | tr 'a-z' 'A-Z')
            current_temp=$(echo "ibase=16 ; scale=1 ; $double_temp/2" | bc)
            printf "current temperature: %s\n" "$current_temp"

            if [ $holiday -eq 1 ] ; then
               parse_holiday_params $7 $8 $9 ${10}
            fi
      elif [ $2 -eq 2 ]; then
         printf "PROFILE CHANGED!\n"
      else
			printf "--error--\n"
		fi
   else
		printf "--error--\n"
	fi
}

#it searches for a valve BY NAME within a file.
#each valve in the file must be represented on a new line with a name
#in the format: NAME/ADDRESS (e.g. bedroom/00:11:22:33:44:55)
#
#input: <valve_name> <file_name>
#output: address of the <valve_name> valve (-1 if it does not exists)
search_by_name() {
	found=0
  search_name=$(echo $1 | tr 'A-Z' 'a-z')
	while read -r line || [[ -n "$line" ]]; do
	    name=$(echo $line | cut -d "/" -f 1 | tr 'A-Z' 'a-z')
	    if [ "$search_name" == "$name" ]; then
	      echo $(echo $line | cut -d "/" -f 2)
			found=1
			break
	   fi
	done < "$2"

	if [ $found -eq 0 ]; then
		echo "-1"
	fi
}
