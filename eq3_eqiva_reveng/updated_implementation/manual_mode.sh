#!/bin/bash

#USAGE:
#	./manual_mode.sh [-p|--parse] valve_name [valve_name ...]
#	puts one or more valves in manual mode.
#	valve's names are supplied as a command line argument.
#	the -p or --parse option activates the parsing of the notifications sent by the valve.

. ./valve_commands.sh

if [ -z $1 ]; then
	printf "Usage: ./manual_mode.sh [-p|--parse] valve_name [valve_name ...]\n"
	exit 1
fi

#check if the parameter -p is present
case $1 in
	-p|--parse)
	parsing=1
	shift #discard the argument

	#check if there is at least another argument
	if [[ -z $1 ]]; then
		printf "Usage: ./manual_mode.sh [-p|--parse] valve_name [valve_name ...]\n"
		exit 1
	fi
	;;

	*)
	parsing=0
	;;
esac

#check if bluetooth is active
check_bt_status

for name in "$@"; do
	address=$( search_by_name $name $VALVE_FILE )

	#check if the address was found and is valid
	if [[ "$address" != "-1" && $(validate_mac $address) != "-1" ]]; then

		#send the command
		notification=$(manual_mode $address)

		#check if notification is NOT an empty string
		if [[ ! -z $notification && $parsing = 0 ]]; then
			printf "\n%s: %s\n" "$name" "$notification"
		elif [[ ! -z $notification && $parsing = 1 ]]; then
			printf "\n%s:\n" "$name"
			parse_return_value $notification
		else
			printf "\n%s: error. try to increase the timeout or move close to the valve\n" "$name"
		fi
	else
		printf "\n%s: not found in '%s' or invalid MAC address\n" "$name" "$VALVE_FILE"
	fi
done
