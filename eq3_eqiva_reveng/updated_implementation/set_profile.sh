#!/bin/bash

#USAGE:
#	./set_profile.sh [-p|--parse] profile_file valve_name [valve_name ...]
#	sets the daily schedules of one or more valves depending on the content of the file.
#	the -p or --parse option activates the parsing of the notifications sent by the valve.
#
#	profile_file contains the schedule related to one or more days according to the
#	following syntax (see example file: week_profile.txt):
#
#	day_name
#	base_temperature
#	HH:MM-HH:MM-TEMP	(1st interval)
#	HH:MM-HH:MM-TEMP	(2nd optional interval)
#	HH:MM-HH:MM-TEMP	(3rd optional interval)
#	end

. ./valve_commands.sh

if [ -z $2 ]; then
	printf "Usage: ./set_profile.sh [-p|--parse] profile_file valve_name [valve_name ...]\n"
	exit 1
fi

#check if the parameter -p is present
case $1 in
	-p|--parse)
	parsing=1
	shift #discard the argument

	#check if there is at least another argument
	if [[ -z $1 ]]; then
		printf "Usage: ./set_profile.sh [-p|--parse] temperature\n"
		exit 1
	fi
	;;

	*)
	parsing=0
	;;
esac

#check if bluetooth is active
check_bt_status

last_end="00:00" ; n_intervals=0 ; base_temp=0 ; n_valves=0

for index in $(seq 2 $#); do
	address=$( search_by_name ${!index} $VALVE_FILE )

	if [[ "$address" != "-1" && $(validate_mac $address) != "-1" ]]; then
		VALVES_ADDR[n_valves]=$address
		VALVES_NAMES[n_valves]=${!index}
		n_valves=$(( $n_valves+1 ))
	else
		printf "\n%s: not found in '%s' or invalid MAC address\n" "${!index}" "$VALVE_FILE"
	fi
done

while read -r line || [[ -n "$line" ]]; do
	if [ -z $line ]; then
		continue #empty line

	#check if line only contains characters
	elif [[ "$line" =~ ^[a-zA-Z]+$ ]]; then

		if [ "$line" == "end" ] || [ "$line" == "END" ]; then
			if [ "$last_end" != "24:00" ] && [ "$last_end" != "00:00" ]; then
				PARAM=$PARAM$base_temp/"24:00"
			fi

			for index in $(seq 0 $(( $n_valves-1 ))); do
				notification=$(set_profile ${VALVES_ADDR[$index]} $PARAM)

				#check if notification is NOT an empty string
				if [[ ! -z $notification && $parsing = 0 ]]; then
					printf "\n%s (%s): %s\n" "${VALVES_NAMES[$index]}" "$day_literal" "$notification"
				elif [[ ! -z $notification && $parsing = 1 ]]; then
					printf "\n%s (%s): " "${VALVES_NAMES[$index]}" "$day_literal"
					parse_return_value $notification
				else
					printf "\n%s (%s): error. try to increase the timeout or move close to the valve\n" "${VALVES_NAMES[$index]}" "$day_literal"
				fi
			done

			#reset "global" variables
			last_end="00:00" ; n_intervals=0 ; base_temp=0

		#line contains the name of the day
		else
			#save the name and convert it to the required format
			day_literal=$line
			day=$( get_day_number $line )
			PARAM=$day" "
		fi

	#in the worst case the temperature is composed of 5 characters (e.g. 20.50)
	elif [ ${#line} -le 5 ]; then
		base_temp=$line

	#last case: the line contains an interval
	else
		if [ $n_intervals -ge 3 ]; then
			echo "warning: too many intervals. some will not be considered."
			continue
		fi

		start=$( echo $line | cut -d "-" -f 1 )
		end=$( echo $line | cut -d "-" -f 2 )
		temp=$( echo $line | cut -d "-" -f 3 )

		if [ "$start" != "$last_end" ]; then
			PARAM=$PARAM$base_temp/$start" "
		fi

		PARAM=$PARAM$temp/$end" "
		last_end=$end
		n_intervals=$(( $n_intervals + 1 ))
	fi
done < "$1"
