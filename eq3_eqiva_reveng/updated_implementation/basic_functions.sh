#!/bin/bash

. ./config.sh

#input: temperature in decimal base.
#output: (temperature*2) in hexadecimal base, rounded to XX.0 or XX.5
calculate_temp() {
    if [ -z $1 ]; then
        echo "Usage: calculate_temp <temperature>"
        exit 1
    fi

    #check if input is less then zero
    if [ $( echo "$1 <= 0" | bc ) -eq 1 ]; then
        exit 1
    fi

    temp=$(awk -v req="$1" 'BEGIN { rounded = sprintf("%.0f", req*2); print rounded }')
    temp=$(printf "%02x" 0x$(echo "scale=0; obase=16 ; $temp/1" | bc))

    echo $temp
}

#input: temperature in decimal base.
#output: (temperature*2)+128 in hexadecimal base, rounded to XX.0 or XX.5
calculate_temp_128() {
    if [ -z $1 ]; then
        echo "Usage: calculate_temp_128 <temperature>"
        exit 1
    fi

    #check if input is less then zero
    if [ $( echo "$1 <= 0" | bc ) -eq 1 ]; then
        exit 1
    fi

    temp=$(awk -v req="$1" 'BEGIN { rounded = sprintf("%.0f", (req*2)+128); print rounded }')
    temp=$(echo "obase=16; $temp" | bc)

    echo $temp
}

#sends a command to the valve
#input: <device_address> <command>
#output: value received after the execution of the command
send_command() {
    output=$(timeout -s INT $TIMEOUT_SEC gatttool -b $1 --char-write-req -a 0x0411 -n $2 --listen)
    ret_value=${output#*: }  #removes the part of the string that precedes ":"
    echo $ret_value
}

#input: bytes 7, 8, 9, 10 of a notification
#output: prints date and time of the end of the holiday (valve in holiday mode)
parse_holiday_params() {
    if [ -z $4 ]; then
        echo "Usage: parse_holiday_params <byte7> <byte8> <byte9> <byte10>"
        exit 1
    fi

    day=$(echo $1 | tr 'a-z' 'A-Z')
    day=$(echo "ibase=16 ; scale=0 ; $day" | bc)
    year=$(echo $2 | tr 'a-z' 'A-Z')
    year=$(echo "ibase=16 ; scale=0 ; $year" | bc)
    month=$(echo $4 | tr 'a-z' 'A-Z')
    month=$(echo "ibase=16 ; scale=0 ; $month" | bc)
    hour=$(echo $3 | tr 'a-z' 'A-Z')
    hour=$(echo "ibase=16 ; scale=0 ; $hour" | bc)

    #the last bit states if minutes are 00 or 30
    last_bit=$(echo "obase=2 ; $hour" | bc)
    length=$((${#last_bit}-1))
    last_bit=`echo ${last_bit:$length:1}`

    minutes=00
    if [ $last_bit -eq 1 ]
    then
        hour=$(( $hour - 1 ))
        minutes=30
    fi

    hour=$(echo "scale=0;$hour/2" | bc)

    #check if parameters are ok
    if  [ $day -lt 1 -o $day -gt 31 ] ||
        [ $month -lt 1 -o $month -gt 12 ] ||
        [ $year -lt 0 -o $year -gt 255 ] ||
        [ $hour -lt 0 -o $hour -gt 24 ]
    then
        exit 1
    fi

    printf "holiday ends: %s at %s\n" "$day/$month/$year" "$hour:$minutes"
}

#input:
#   content of the notification received from the valve
#   note: each byte is represented by a different parameter
#output:
#   translates the notification into "readable" content
parse_return_value() {
    #$1 = 0x21 = 33 -> received daily schedule
    #$1 = 0x02 = 02 -> received another type of notification
    notify=$(echo "ibase=16 ; $1" | bc )

    if [ $notify -eq 33 ]; then
        temp_param=3
        end_time_param=4
        last_interval=0

        day=$( echo "ibase=16 ; $2" | bc )
        if [ "$day" -lt 0 -o "$day" -gt 6 ]; then
            printf "Error: day of the week out of range\n"
            exit 1
        fi

        while [ $end_time_param -lt 17 -a $last_interval -ne 1 ];
        do
            temperature=$(echo ${!temp_param} | tr 'a-z' 'A-Z')
            end_time=$(echo ${!end_time_param} | tr 'a-z' 'A-Z')
            temperature=$(echo "ibase=16 ; scale=1 ; $temperature/2 " | bc)
            end_time=$(echo "ibase=16 ; $end_time" | bc)
            end_time=$(( $end_time * 10 ))

            r=$(( $end_time % 60 ))
            end_time=$(( $end_time/60 ))
            printf "%s degrees until %02d:%02d\n" "$temperature" "$end_time" "$r"

            if [ $end_time == "24" -o $end_time == "00" ]; then
                last_interval=1
            else
                temp_param=$(( $temp_param + 2 ))
                end_time_param=$(( $end_time_param + 2 ))
            fi
        done
    elif [ $notify -eq 2 ]; then
        #$2=01 -> information about valve' status
        #$2=02 -> successful schedule change
        if [ $2 -eq 1 ]; then
            #check the third byte of the return value
            active_mode=$(echo $3 | tr 'a-z' 'A-Z')
            first_bit=$(echo ${active_mode:0:1})
            active_mode=$(echo "ibase=16 ; $active_mode" | bc)

            if [ $first_bit -eq 2 ] ; then
                #command block on ($active_mode=28/29/2A/2C/2D/2E)
                #subtract 32 (0x20) to go back to the prevous cases (08/09/0A etc...)
                printf "//LOCKED!//\n"
                active_mode=$(( $active_mode - 32 ))
            fi

            holiday=0
            case "$active_mode" in
                8) printf "AUTO MODE\n";;                #auto mode 08
                9) printf "MANUAL MODE\n";;              #manual mode 09
                10)   printf "HOLIDAY MODE\n"            #vacation mode 0A
                holiday=1;;
                12)   printf "BOOST (auto mode)\n";;     #boost mode 0C -> end boost go to auto
                13)   printf "BOOST (manual mode)\n";;   #boost mode 0D -> end boost go to man
                14)   printf "BOOST (holiday mode)\n"    #boost mode 0E -> end boost go to holiday
                holiday=1;;
            esac

            #sixth byte of the ret value contains (temperature*2)
            double_temp=$(echo $6 | tr 'a-z' 'A-Z')
            current_temp=$(echo "ibase=16 ; scale=1 ; $double_temp/2" | bc)
            printf "current temperature: %s\n" "$current_temp"

            if [ $holiday -eq 1 ] ; then
                parse_holiday_params $7 $8 $9 ${10}
            fi
        elif [ $2 -eq 2 ]; then
            printf "Profile changed!\n"
        else
            printf "Error: unknown notification\n"
            exit 1
        fi
    else
        printf "Error: unknown notification\n"
        exit 1
    fi
}

#it searches for a valve BY NAME within a file.
#each valve in the file must be represented on a new line with a name
#in the format: NAME/ADDRESS (e.g. bedroom/00:11:22:33:44:55)
#
#input: <valve_name> <file_name>
#output: address of the <valve_name> valve (-1 if it does not exists)
search_by_name() {
    if [ -z $2 ]; then
        echo "Usage: search_by_name <valve_name> <file_name>"
        exit 1
    fi

    found=0
    search_name=$(echo $1 | tr 'A-Z' 'a-z')
    while read -r line || [[ -n "$line" ]]; do
        name=$(echo $line | cut -d "/" -f 1 | tr 'A-Z' 'a-z')
        address=$(echo $line | cut -d "/" -f 2)
        if [ "$search_name" == "$name" -a "$address" != "" ]; then
            found=1
            break
        fi
    done < "$2"

    if [ $found -eq 0 ]; then
        echo "-1"
    else
        echo $address
    fi
}

# check if a MAC address is valid
# input: <MAC_address>
# output:
#   0 if valid
#   -1 otherwise
validate_mac() {
    #check the presence of the required parameter
    if [[ -z "$1" ]]; then
        echo "Usage: validate_mac <MAC_address>"
        exit 1
    fi

    #check if $1 is a valid mac address
    if [[ "$1" =~ ^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$ ]]; then
        echo "0"
    else
        echo "-1"
    fi
}

#check if Bluetooth is active
#input: -
#output: an error message if not active
check_bt_status() {
    bt_active=0
    hci_down_list=""

    hciconf_output=$(hciconfig)

    while read line
    do
        #search for lines starting with "hciX"
        if [[ "$line" =~ ^hci[0-9] ]]; then
            #get the name of the bluetooth adapter
            adapter_name=$(echo "$line" | cut -f 1)

            read #skip a line
            read adapter_status

            #check if the adapter is in UP or DOWN state
            if [[ $adapter_status == *"UP"* ]]; then
                bt_active=1
                break
            else
                #save name and status of the (disabled) adapter
                hci_down_list="$hci_down_list$adapter_name $adapter_status"$'\n'
            fi
        fi
    done <<< "$hciconf_output"

    #no active adapter found. print a list of "adapter_name: status"
    if [ "$bt_active" -eq 0 ]; then
        echo "ERROR: bluetooth is disabled"
        echo
        echo "List of adapters found:"
        echo "$hci_down_list"
        exit 1
    fi
}

#convert the name of a day to a number, according to the format required by the valves
#input: name of the day in English. NOT case sensitive.
#output: 0=Saturday .. 6=Friday
get_day_number(){
    if [ -z $1 ]; then
        echo "Usage: get_day_number <day_name>"
        exit 1
    fi
    
    #date command returns 0=Sunday ... 6=Saturday
    day=$( date --date="$1" +"%w" )

    #exit if "date" finished with errors or $1 contains digits
    if [[ $? != "0" || ! $1 =~ ^[a-zA-Z]+$ ]]; then
        exit 1
    fi

    if [[ $day != "0" ]]; then
        echo $(( ($day + 1) % 7 ))
    else
        echo $(( $day + 1 ))
    fi
}
