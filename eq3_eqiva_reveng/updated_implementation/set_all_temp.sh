#!/bin/bash

#USAGE:
#	./set_all_temp.sh [-p|--parse] temperature
#	changes the temperature of ALL the valves listed in $VALVE_FILE
#	the -p or --parse option activates the parsing of the notifications sent by the valve.

. ./valve_commands.sh

if [ -z $1 ]; then
	printf "Usage: ./set_all_temp.sh [-p|--parse] temperature\n"
	exit 1
fi

#check if the parameter -p is present
case $1 in
	-p|--parse)
	parsing=1
	shift #discard the argument

	#check if there is at least another argument
	if [[ -z $1 ]]; then
		printf "Usage: ./set_all_temp.sh [-p|--parse] temperature\n"
		exit 1
	fi
	;;

	*)
	parsing=0
	;;
esac

#check if bluetooth is active
check_bt_status

#for each valve in $VALVE_FILE
while read -r line || [[ -n "$line" ]]; do
	#get name and address
	name=$(echo $line | cut -d "/" -f 1)
	address=$(echo $line | cut -d "/" -f 2)

	#check if the address is valid
	if [[ $(validate_mac $address) != "-1" ]]; then

		#send the command
		notification=$(set_temperature $address $1)

		#check if notification is NOT an empty string
		if [[ ! -z $notification && $parsing = 0 ]]; then
			printf "\n%s: %s\n" "$name" "$notification"
		elif [[ ! -z $notification && $parsing = 1 ]]; then
			printf "\n%s:\n" "$name"
			parse_return_value $notification
		else
			printf "\n%s: error. try to increase the timeout or move close to the valve\n" "$name"
		fi
	else
		printf "\n%s: invalid MAC address\n" "$name"
	fi
done < "$VALVE_FILE"
