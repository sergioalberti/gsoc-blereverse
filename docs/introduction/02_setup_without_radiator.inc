Setup Without A Radiator
-------------------------

Once mounted on the radiator, the valves must perform an *adaptive run* phase before they can be used.
During this phase the valve lengthens the external pin until it detects the radiator's pin and calibrates itself (on the strength of the valve pin).
While this activity takes place, the ``AdA`` message is shown on the display and no further operations can be performed.

.. (atrent) citare messaggio che viene visualizzato se la calibrazione fallisce e cosa bisogna fare per rifarla (per spiegare l'utilità del "simulating device")
.. .. [CHISUO] (sergioalberti) in sostanza era scritto nella nota a fondo pagina. l'ho spostata qui che effettivamente ha piu' senso (per non scriver due volte la stessa cosa)

.. _valve_errors:

.. note::

    According to the manual [2]_, if during the setup phase the valve shows the error messages ``F2`` or ``F3``
    the reasons are respectively:

    * the valve pin extended to the maximum length without meeting the radiator pin
    * the valve pin was blocked before reaching the minimum required distance

    While the error is shown, the motor returns to its starting position (indicated by ``Ins`` on the display).
    To restart the calibration, press the button in the middle of the valve and wait again for the necessary time.

.. (atrent) raccontare del modo "artigianale" col pennarello tenuto saldamente con la mano?
.. .. [CHIUSO] (sergioalberti)

However, it may happen that there isn't a radiator suitable for initialization or an immediate way to
perform this phase. Several times we have done the calibration by inserting a marker in the valve and locking it against
a wall to exert the necessary force. It is easy to understand that this is *not* a very convenient method and often
requires more attempts.

Since the valve must recognize that it is in contact with something *very constantly resistant*, alternative methods can be found.
The least expensive solution involves using the **adapter** supplied with the valves. It is designed to make them usable
on various types of radiators and is equipped with a screw to tighten and loosen the grip (see :ref:`adapter_details`).

.. _adapter_details:
.. figure:: ../images/adapter.png
    :scale: 50%
    :alt: adapter details
    :align: center

    Adapter Details

The second piece we need is a **large screw** (or a sort of cylinder) of the size needed to **fill the adapter**.
(see the :ref:`next image <test_device1>` as a reference).
It is important that when the adapter is tightened the screw makes a lot of grip because the valve's pin pushes very hard.

.. (atrent) frase non chiara, dimmela in italiano
.. (sergioalberti) in effetti non si capiva qual'era il soggetto. ho provato a sistemarla.
.. (sergioalerti) sarebbe: "è importante che quando l'adattatore viene chiuso la vite faccia molto attrito perchè il pin della valvola spinge con molta forza"

The bolt can be found in many hardware stores at low cost (*less than 1€*), while the adapter will cost you more (*between 5€ and 10€*).
However it is usually included in the valve package.

.. _test_device1:
.. figure:: ../images/test_device1.png
    :scale: 50%
    :alt: test device pieces
    :align: center

    From *left to right*:
    the valve, the screw and the adapter

Now it is enough to:

1. insert the screw in the adapter so that it sticks out a lot towards the valve
2. tighten the small screw around the adapter
3. attach the adapter to the valve (see :ref:`test_device_anim`)
4. start the setup phase by pressing the big button on the valve

.. (atrent) fare un video? (o una gif animata)
.. .. [CHIUSO] (sergioalberti) ci provo!

.. _test_device_anim:
.. figure:: ../images/test_device_animation.gif
    :scale: 70%
    :alt: mounted device
    :align: center

    Test device setup example

If during the calibration phase we meet one of the *errors* mentioned in the :ref:`previous note <valve_errors>`,
then it's sufficient to **adjust the screw** accordingly.

---------------------------------------------

.. [2] `Eq3 Eqiva User Manual <https://www.eq-3.com/Downloads/eq3/downloads_produktkatalog/eqiva/bda/CC-RT-BLE-EQ_UM_GE_eQ-3_160428.pdf>`_
