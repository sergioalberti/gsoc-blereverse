What Radiator Valves Are
-------------------------
Radiator valves are thermoregulation devices composed of two parts: a valve and a bulb
in contact with the surrounding environment. The bulb contains a fluid with a high expansion coefficient.
The set of these two components allows to create a system that, according to a range of values placed on the valve,
expands or contracts the fluid. This causes the activation or the interruption of the flow to be managed.
It is therefore clear that the application area is that of heating and cooling systems. The use of thermostatic
valves on domestic heaters has become fundamental because it allows the reduction of consumption and emissions.

.. (atrent) mettere uno schemino grafico del principio di regolazione?
.. .. [CHIUSO] (sergioalberti)

.. figure:: ../images/nonble_schema.png
    :alt: Operating scheme of a classic valve
    :align: center

    Operating scheme of a classic valve [1]_

Electronic versions of these valves have been available for some years now.
They are battery powered
and equipped with an integrated thermostat replacing the classic fluid.
Theese valves can be programmed for the whole week and allow a more accurate selection of the desired temperature.
For ease of use, the interaction with these valves usually takes place through
a proprietary smartphone application that uses a Bluetooth or Wi-Fi connection for
data exchange.

.. (atrent) mettere uno schemino grafico del sistema con lo smartphone aggiunto?
.. .. [CHIUSO] (sergioalberti)

.. figure:: ../images/ble_schema.*
    :alt: Electronic BLE valve
    :align: center

    Electronic BLE valve

--------------------------------------------

.. [1] `Demshop - Caleffi 200 <https://www.demshop.it/TESTA-TERMOSTATICA-CALEFFI-200>`_
