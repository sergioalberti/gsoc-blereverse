Android Application Analysis
============================
This section wants to give a guideline and provide some reference examples about:
* which services to use to decompile an Android application
* how to analyze the aspects of communication and data transmission via Bluetooth

The fact that there is no common standard to follow in the creation of Android applications makes it difficult
to generalize the code and the analysis.

.. (atrent) esatto, non credo sia facile, ma sarebbe importante tentare di dare consigli generali su come affrontare la /reveng/ di una generica app, tipo una specie di workflow/checklist, con consigli su che tipo di classi andare a cercare (i.e., quelle in cui si usano le lib di accesso al bluetooth)
.. .. [CHIUSO] (sergioalberti) visto anche il commento sotto, questo punto lo considero fatto? nel senso che in una checklist non saprei bene cosa scrivere oltre ciò che ho riportato nele varie sezioni, però l'esempio (sez 2.3.5) mi sembra abbastanza passo/passo

.. (atrent) usare strumenti di analisi automatica del codice? tipo roba che ti crea uno schema delle classi con i reference tra una e l'altra (Class Diagram, cerca "java class diagram generator")
.. (sergioalberti) ho visto che esiste anche un plugin per Android Studio. lo provo!
.. .. [CHIUSO] (sergioalberti) aggiunta sezione "Create A Class Diagram"

.. (atrent) Android Studio è libero? (vedi anche altra nota generale più sotto)
.. .. [CHIUSO] (sergioalberti) sì! ho aggiunto un link al codice sorgente.

In general, to get the source code you need the **APK package** of the application, which is basically an archive that contains
all the data that Android needs to manage the installation of the app. Once in possession of the APK file, there
are decompilers (typically for Java code, on which Android is based) that in a short time generate an archive
containing the original code. The product code is automatically identified as "Android project" by `Android
Studio <https://developer.android.com/studio/>`_, which is the official
`open source <https://sites.google.com/a/android.com/tools/build>`_ *IDE* for the development of Android
applications. This ensures ease of reading and analysis, taking advantage of the advanced development features
provided by the software.

.. _obtain_apk:

Get The APK Package
--------------------

.. (atrent) mi pare che anche i programmi di backup (tipo titanium) estraggano gli apk
.. (sergioalberti) non ho ancora avuto modo di rootare il mio telefono, a breve aggiorno (titanium richiede root)
.. .. [CHIUSO] aggiunto un paragrafo. ho utilizzato altre applicazioni, che non richiedono root

**Option 1: through a web service**

A fairly simple way to perform this step is to rely on a web service to extract from the Google Play Store the APK
package of the application used by the BLE device. The `APKPure <https://apkpure.com/>`_ site is the only one on
which we have found our reference application, but there are also other sites that provide the same service, such as
`App Apk <https://www.appsapk.com>`_ and `APKSFree <https://www.androidapksfree.com>`_.

If none of these sites leads to a result, it is often possible to obtain the APK package through the procedures
described in the next section.

**Option 2: through a File Manager app**

Also the *File Manager* applications often allow to extract the APK packages from the applications on the device.
We mention this option as "second possibility" because in the medium case these applications require a lot of permissions
to operate (e.g. read storage, read phone status). We report the procedure to be performed with two different applications:

`ES File Manager (release 4.1.8.1) <https://play.google.com/store/apps/details?id=com.estrongs.android.pop>`_

1. from the application's main page (called "Home"), select *"APP"*
2. identify the application for which you want to get the apk
3. press and hold the icon, then click on "backup"
4. the app tells you where the apk file is saved (in our case ``/sdcard/backups/apps``)

`Astro File Manager (release 6.4.0) <https://play.google.com/store/apps/details?id=com.metago.astro>`_

1. from the application's main page (called "File Manager"), select *"Go To App Manager"*
2. identify your application, hold the icon and select "backup" from the drop-down menu
3. an apk file is created and it can be accessed from the file manager itself
   (it does not say where it is saved; in our case in ``/sdcard/backups/apps``)

As you can see, the procedures are very similar and also on other applications will not be very different.
Note that probably the well-known `Titanium Backup <https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup>`_
allows you to do this. We did not consider it for simplicity, as it requires the device to be rooted.

**Option 3: through ADB (manual method)**

Assuming you have the application installed on your smartphone, you can get the APK package through **adb**
(*Android Debug Bridge*), which is a command-line tool that allows you to control an Android smartphone via USB.
The adb tool can be installed through the ``android-adb-tools`` package, available in the Debian repositories.

.. note::
    ``adb`` requires USB debugging to be enabled. This function can be found in the "Developer Options" section.
    (refer to :ref:`this note <developer_options>` if there is no “Developer Options” entry in the settings of
    your smartphone)

Once installed adb and connected the device via USB, use the following commands:

.. code-block:: shell

    $ adb shell pm list packages          #find the package name of the app
    $ adb shell pm path package-name      #find the address of the app
    $ adb pull app-address                #copy the apk

Depending on the Android version used, the following error may be returned:

:code:`remote object "app-address" does not exists`

In this case, you need to move the apk file to an accessible folder before downloading it.
Use the following commands:

.. code-block:: shell

    $ adb shell cp app-address /storage/emulated/0/Download
    $ adb pull /storage/emulated/0/Download/base.apk

The apk file ``base.apk`` should now be in your home and contains the necessary to get the Java code.

.. _obtain_source:

Obtain the source code
-----------------------

.. (atrent) magari citare una lista di decompilatori (su wikipedia mi pareva ci fosse)
.. .. [CHIUSO] (sergioalberti) mmm, non l'ho trovata. c'è una pagina "Java Decompiler" che però è il nome proprio di un decompilatore. ne ho cercati alcuni funzionanti e ho integrato la sezione sotto.

Using the APK file we now want to extract the Java code that makes up the application. Before proceeding it
is good to know that there are cases in which the code will be obfuscated. *Code obfuscation* is a practice
that consists in making the code more complicated without changing its functionality in order to make it
more difficult to understand. This is done to avoid reverse engineering practices.

There are some open source Java de-obfuscators, such as `Java Deobfuscator <https://javadeobfuscator.com/>`_ (and his
`GUI <https://github.com/java-deobfuscator/deobfuscator-gui>`_) or `Enigma <http://www.cuchazinteractive.com/enigma/>`_.
They require a *Jar* file as an input, which can be obtained from the APK by following the first two points of
:ref:`this <option2_decompiler>` section. However, their use will not be covered in this guide.

.. (atrent) citarne almeno qualcuno
.. .. [CHIUSO] (sergioalberti)

.. _option1_decompiler:

**Option 1: through a web service**

As before, we can delegate the work to web services and more than one site can be useful.

Among the sites tested, `Java Decompilers <http://www.javadecompilers.com/apk/>`_ is the one that provided the
clearest code: probably the most similar to the real one (variable names are reasonable and there are no ``GOTO``
statements). `ApkDecompilers <https://www.apkdecompilers.com/>`_ produces the same result, but puts all the
files in a single directory. Since Android applications include a lot of files, it is less comfortable.

.. _option2_decompiler:

**Option 2: through dex2jar and JD-GUI (manual method)**

*Dex2jar* and *JD-GUI* are two programs that respectively allow to transform the apk file into a *Jar*
(Java Archive) and decompile it by following these steps:

1.  Download the latest release of *dex2jar* from `this page <https://github.com/pxb1988/dex2jar/releases>`_

    The file to be downloaded is called ``dex-tools-X.X.zip``, where X.X indicates the version number
    (2.0, at the time of writing)

2.  Execute these commands in a terminal:

    .. code-block:: shell

        $ unzip dex-tools-X.X.zip
        $ cd dex2jar-X.X
        $ chmod u+x *.sh
        $ ./d2j-dex2jar.sh /path/to/application.apk    #the application APK

    This produces a jar file in the ``dex-tools-X.X`` directory.

3.  Download and install `JD-GUI <http://jd.benow.ca/>`_ (released under the GPLv3 license)
4.  Open *JD-GUI* and select ``File > Open File`` in order to open the jar archive produced with dex2jar.
5.  Select ``File > Save All Sources``. This produces a zip archive containing all the Java files.

JD-GUI has the advantage of clearly showing the *Java packages* that make up the project. However, individual
files are less clear than those produced with the service discussed in the :ref:`Option 1 <option1_decompiler>`.

Other decompilers can be used instead of JD-GUI. One of this is `CFR <http://www.benf.org/other/cfr/>`_, which
is *not open source* but it also decompiles modern **Java 9 features**. Other good solutions are
`Fernflower <https://github.com/fesh0r/fernflower>`_ and the one included in the
`Procyon suite <https://bitbucket.org/mstrobel/procyon/>`_.

Import In Android Studio
-------------------------

.. (atrent) serve per forza android studio o si può usare un normale editor?
.. .. [CHIUSO] (sergioalberti) in effetti no. metto una nota.

Regardless of the method chosen, the files obtained can be imported into Android Studio to be analyzed.
Both :ref:`Option 1 <option1_decompiler>` and :ref:`Option 2 <option2_decompiler>` produce a zip file.

.. note::
    Obviously, you can use any text editor to analyze the produced files. This guide focuses on Android Studio as
    it's an *open source* tool and now a standard in creating Android applications. Whatever the choice, we strongly
    suggest the use of an IDE that integrates well with Java. (e.g `Eclipse <https://www.eclipse.org/ide/>`_,
    `NetBeans <https://netbeans.org/>`_).

.. _android_studio_import:
.. figure:: ../images/android_studio_import.*
    :alt: android studio import project
    :align: center

    Android Studio - Import Project

Once extracted the zip file, open Android Studio and follow these steps:

1. ``File > New > Import Project``
2. Select the directory in which the archive was extracted (the one in which the *XML Manifest* file is present) and
   click ``Next`` (see :ref:`android_studio_import`).
3. ``Create project from existing sources > Next``
4. Choose a name for the project and the directory in which to save it (personal choices). Then click ``Next``.
5. All the default settings should be fine, so keep clicking ``Next``.

After that, a panel on the left allows you to navigate between packages and files, while the right side
shows an editor enabled for grammatical (and syntactic) correction.

What to look for in the code
----------------------------

.. (atrent) citare qualche testo/link sul reveng?
.. .. [CHIUSO] (sergioalberti) ho inserito una sezione a fondo pagina con link ad altre guide simili a questa. non trovo testi veri e propri relativi al reveng ble.

.. (atrent) si potrebbe fare analisi/debug facendo girare l'app in un emulatore?
.. (sergioalberti) con l'emulatore ufficiale no perchè non supporta il bluetooth. provo questa alternativa (appena rientro in possesso del mio adattatore bluetooth, domani) https://chrislarson.me/blog/emulate-android-and-bluetooth-le-hardware/
.. .. [CHIUSO] (sergioalberti) il mio adattore non viene visto da virtualbox e a quanto pare con il bluetooth interno non è possibile. metto comunque una nota a inizio guida!

In general, a good way to avoid having to consult all the files in the project is to proceed by keywords
and use a search tool. In android studio this tool is provided in the ``Edit > Find > Find In Path`` menu.
For example, the :ref:`next <android_studio_find>` image shows all the files containing the search
keyword *OnLeScan*.

.. _android_studio_find:
.. figure:: ../images/android_studio_find.*
    :alt: Android Studio "Find in path" window
    :align: center

    Android Studio "Find in path" window

Within the reverse engineering activity there is little interest in understanding how the connection to
the target BLE device works. We can therefore ignore the details of the bluetooth connection to focus
on two things:

1. identify the files that create the **strings that will be sent to the peripheral** (made of hexadecimal values), to see how they are composed
2. identify the files that manage **received notifications** to understand how they are interpreted

If you want to find Java classes that deals with the act of Bluetooth connection, you can identify useful keywords
(such as ``BluetoothAdapter``, ``startLeScan``, ``LeScanCallback``, ``BluetoothGatt``) by referring to the
`Android BLE API page <https://developer.android.com/guide/topics/connectivity/bluetooth-le>`_.

.. (atrent) ecco, immaginavo infatti ;)

**Commands**

Finding the classes and methods that deal with composing commands (i.e. *strings* of values) is not
straightforward. This is because every manufacturer can act without constraints when he implements his own proprietary
coding. It is not even necessary for these classes to contain keywords related to Bluetooth, as they could be
treated separately.

In the best case, the application will contain Java packages with names like *command*, *sendCommand*,
or something similar and this facilitates the search. Otherwise, the criterion to be used, consists of:

*   to use as search keywords the name of the methods that Android uses to write BLE characteristcs
*   starting from the results found, **go back** to the classes used for the generation of commands

Specifically, Android within the `BluetoothGatt <https://developer.android.com/reference/android/bluetooth/BluetoothGatt>`_
class provides the ``writeCharacteristic`` method. Note that the values we are interested in are those that must be written
on the characteristics. To set a value before writing it, the
`BluetoothGattCharacteristic <https://developer.android.com/reference/android/bluetooth/BluetoothGattCharacteristic>`_
class provides the ``setValue`` method.

As a result, ``command``, ``writeCharacteristic`` and ``BluetoothGattCharacteristic`` are examples of good search keys.

.. _what_to_look_notifications:

**Notifications**

Notification management can also be done in various ways depending on the choices of the producers. However, as for commands,
at some point the application must definitely use the default methods provided by Android to manage the reception.

Hypothetically, once received and extracted the content of the notification, this is sent to a sort of *parser* that
interprets it and then performs other tasks depending on its meaning. This parser is what we are interested in.

To easily identify files that contain useful code, it is important to know that:

* the receipt of notifications by a feature must be **explicitly enabled**, using the ``setCharacteristicNotification`` method
  of the `BluetoothGatt <https://developer.android.com/reference/android/bluetooth/BluetoothGatt>`_ class
* the callback function called when a notification is received is ``onCharacteristicChanged``, provided by the
  `BluetoothGattCallback <https://developer.android.com/reference/android/bluetooth/BluetoothGattCallback>`_ class

This callback function will be the starting point to go back to the pieces of code that actually interpret
the content of the notifications.

In conclusion, good search keys are: ``setCharacteristicNotification``,
``onCharacteristicChanged`` and ``BluetoothGattCallback``.

Create A Class Diagram
-----------------------

It is often useful to have a Class Diagram [6]_ available for the whole project or just for small parts. This allows you to
schematically represent the application and **highlight the dependencies** between the classes that compose it.

Manually creating a Class Diagram for an Android application can result in a lot of work, so some automated tools will help us.
There are several Class Diagram generators for Java/Android code, but we will explain how to install and use
`Code Iris <https://plugins.jetbrains.com/plugin/7324-code-iris>`_ because:

.. (atrent) in *generale* suggerire sempre software libero (non ho guardato se questo lo è), se per caso si deve usare uno strumento proprietario bisogna ESPLICITARE che non è libero e SPIEGARE il motivo della scelta
.. .. [CHIUSO] (sergioalberti) in effetti NON lo è, sto cercando alternative libere funzionanti. aggiorno appena trovo.

* integrates easily into Android Studio
* allows to *filter* and *highlight* classes and packages (useful for big projects)
* it is quite frequently updated
* allows to export data in Json format

.. note::
    *Code Iris is not open source*. However, after trying different free alternatives, our opinion is that
    it is the easiest and most complete solution. If you want to stick with free software, skip to
    :ref:`this <free_alternative_class_diagram>` paragraph.

A few steps are required to install *Code Iris*:

1. open Android Studio and select ``File > Settings > Plugins``
2. click on ``Browse Repositories`` on the bottom of the window
3. search for ``code iris`` and click on the green ""Install"" button
4. restart Android Studio

.. _start_codeiris:
.. figure:: ../images/code_iris_start.*
    :alt: code iris first start
    :align: center

    Code Iris First Start

Once you restart Android Studio and open a project, you can start *Code Iris* through the corresponding tab at the top right
of the window. The first time you need to create the Class Diagram via the "Create/Update Diagram" button (see :ref:`start_codeiris`).

The operation takes a few moments, but produces a Class Diagram related to the *whole project*. The result can be inspected via
three different "views":

* **Module View**: the most abstract view, generally not very useful for our purpose
* **Package View**: maintains the subdivision into packages and also shows the classes they contain
* **Class View**: shows all classes without the subdivision into packages

The *Package View* and the *Class View* are both useful, depending on personal needs. The large size of Android projects,
however, requires the ability to "cut" parts of the Class Diagram, so that it becomes easy to read.

Code Iris provides filtering tools according to **class name** and/or **package name**. This allows us to identify useful parts of
the Class Diagram. Once this is done, moving the cursor on the names of the individual classes highlights all their dependencies
(see :ref:`filtering_codeiris`).

.. _filtering_codeiris:
.. figure:: ../images/code_iris_filtering.png
    :alt: code iris filtering
    :align: center

    Class View with Filtering Enabled

NOTE: the version of Code Iris in the Android Studio repositories is from 2014. More recent versions (updated to 2018) can be
easily installed by downloading the package from the `JetBrains <https://plugins.jetbrains.com/plugin/7324-code-iris>`_ site
and through the "Install Plugin From Disk" option in ``File > Settings > Plugins`` (inside Android Studio).

.. _free_alternative_class_diagram:

**A free and open source alternative**

Among the open source alternatives there are many software that can generate class diagrams [8]_ [9]_ [10]_ [11]_. However, they often present
problems such as:

* Request to *manually* add each class to the diagram. It does not make much sense because we don't know how
  the application is composed. It's more important to be able to add individual packages.
* Request to *manually* select each Java source directory. Not ideal, given the size of Android projects.
* Very confusing graphic interface.

An acceptable solution involves the use of `NetBeans IDE <https://netbeans.org/>`_, which is a Java IDE [7]_ , and
the `EasyUML <http://plugins.netbeans.org/plugin/55435/easyuml>`_ plugin. Since *NetBeans* is already present in
the Debian repositories, you can install it with `apt`::

    $ sudo apt install netbeans

EasyUML requires you to download the correct package, which depends on the NetBeans version, from
`this <http://plugins.netbeans.org/plugin/55435/easyuml>`_ page. To install it you need to extract the files
from the downloaded *zip* archive, open *NetBeans*, select ``Tools > Plugins > Downloaded > Add Plugins`` and choose
all the ``.nbm`` files from the extracted folder.

Once the software is working, both a Java project and a UML project must be created in order to obtain a Class Diagram.
The following is a step by step guide:

1. Create a **Java project** from existing sources:
    * ``New > Project > Java > Java project with existing sources > Next``
    * set the name of the project and when asked for "Source Package Folders" select
      the directory in which the APK' source code have been extracted
    * if the message "The specified package folder contains compiled class files" appears, click on `Ignore`

2. Create an **UML** project:
    * ``New > Project > UML > UML Diagrams Project > Next``

3. Now the left panel shows the Java *packages*. Right click on the package you want to create the Class Diagram.
   and choose "easyUML Create Class Diagram".

4. Choose the easyUML project created before and click on "Create Class Diagram"

The generated Class Diagram contains *by default* more information (methods and members) than the one shown in
the previous section. This makes it more detailed, but messy and often unclear.

To keep only the names of the classes (and their relationships) make a right click on the diagram and remove
the check from "Show Members" under "Visual Options". The following image shows the obtained diagram. For comparison
purposes, the code part shown in the diagram is equivalent to the one shown in figure :ref:`filtering_codeiris`.

.. figure:: ../images/netbeans_diagram.png
    :alt: netbeans and easyUML
    :scale: 25%
    :align: center

    Netbeans And EasyUML - Class Diagram

Example Application Analysis
----------------------------

The examples shown will temporarily refer to the *CalorBT* application, supplied with the radiator valves discussed
in the :doc:`introduction <../introduction/00_introduction>`. They will be subsequently extended to other devices as soon as they are
tested.

The *CalorBT* application, developed by eQ-3, is available for Android 4.4+ and iOS 8.3+ platforms. The analyzed
version is the 1.1.7 updated to the month of January 2016 (currently the last available), for which the source code
was obtained using the *APKPure* and *JavaDecompilers* web services, as described in the previous sections (see
:ref:`obtain_apk` and :ref:`obtain_source`).

.. _calorbt_screen:
.. figure:: ../images/calorbt_screen.*
    :alt: calorBT screenshot
    :align: center

    Main Activity (left) and Weekly Schedule (right)

The application is quite simple and the initialization phase only requires to pair the central device with the
desired radiator valves through a simple pairing procedure. It is interesting to note that it's allowed to
communicate with only **one device at a time**. Once the connection is made, you are sent to a temperature management
activity (see :ref:`Main Activity <calorbt_screen>`), from which you can access all the other features.

Once the project has been imported into Android Studio, it's immediately obvious that the source code is *free of
obfuscation*. The files are completely readable and mutually consistent. Furthermore, the decompiler maintained the
package subdivision of the entire application.

This is good news, it suggests that the manufacturer has not tried to prevent reverse engineering works.
Nevertheless, the analysis is not immediate: excluding external libraries (such as *ButterKnife* and *FasterXML*)
the application consists of 1398 files including **144 classes** and **16 Java interfaces**. Given such a large number
of files, it will be necessary to use the search methods discussed in the previous sections.

**Commands**

Proceeding as described in the previous section, a short search with the keyword ``command`` shows the existence of a
series of Java classes that form the code needed to compose the commands (see :ref:`command_search_results`).
They are contained in the ``de.eq3.ble.android.api.command`` package (*a Java class for each command*).

.. _command_search_results:
.. figure:: ../images/find_command.png
    :alt: search results
    :scale: 60%
    :align: center

    Search results

This type of organization is particularly helpful as it allows to know all the instructions that the application can send
to the radiator valve and for each one provides detailed information. Each class has been designed according to the same
principle:

* the constructor deals with composing and storing the value corresponding to the instruction in an array

* external activities can request this value through the public method ``getCommandData``, which is certainly present
  as required by the ``IThermostatCommand`` interface implemented by each class.

The example seen in the ":ref:`operations_with_params`" section refers to the ``SetTemperatureCommand`` class shown below.

.. code-block:: java
    :linenos:
    :class: code-with-numbers

    public class SetTemperatureCommand implements IThermostatCommand {

        private final byte[] commandData;

        public SetTemperatureCommand(Number setPointTemperature) {
            this.commandData = new byte[2];
            this.commandData[0] = (byte) 65;
            this.commandData[1] = (byte) ((int)
                (setPointTemperature.doubleValue() * 2.0d));
        }

        public byte[] getCommandData() {
            return this.commandData;
        }
    }

In summary, the logging activity had identified:

* the byte ``0x41`` as a common pattern to all instructions of this type
* a second byte as a variable part, equivalent to the chosen temperature *doubled*

Line 6 of the above code shows that the command will consist of 2 bytes, the content of which is shown in lines 7 and 8.
The first always contains the value 65, whose conversion in hexadecimal corresponds precisely to ``0x41``. The second one,
on the other hand, is variable. It's based on the ``setPointTemperature`` parameter supplied to the class constructor and
reveals the temperature coding already mentioned.

It is interesting to note that line 8 not only provides information on the meaning of the second byte, but also indicates
**how to calculate** it. This is particularly helpful in creating the instructions within the management software discussed
later, so that they can be consistent with what is required by the valve.

**Notifications**

Referring to what was said in the :ref:`previous section <what_to_look_notifications>`, we look for the function ``onCharacteristicChanged``. This is
located in the file ``BLEGattCallback.java``. Looking at the code, we see that the content of the notification is interpreted by the
``updateDeviceState`` function, whose prototype is:

.. code-block:: java

    public void updateDeviceState(String deviceId, byte[] value)

The function is located in the file ``BluetoothAPI.java`` and, as for commands, the data are
stored in a *byte array* (represented by the second argument ``value``).

``updateDeviceState`` recognizes the type of notification by reading the value contained in the first and, in cases of
ambiguity, in the second byte. Based on this, it delegates the correct operations to other methods. This creates a first
subdivision, corresponding to the one mentioned in the :ref:`logging section <logging_notifications_example>`. More
detailed information is then obtained by analyzing function calls.

The method adopted is therefore very simple. Assuming, for example, to send a request to read a daily profile to the
radiator valve, the result will consist of a notification whose first byte will correspond to the value ``0x21``
(i.e. 33, in the decimal system). Within the ``updateDeviceState`` function, which will not be reported entirely for
space reasons, this is the part of code involved:

.. code-block:: java
    :linenos:
    :class: code-with-numbers

    int frameType = value[0] & 255; //extract the first byte
    ..
    if (frameType == 33) {

        dayOfWeek = ModelUtil.getDayOfWeek(value[1]);
        byte[] profileData = new byte[(value.length - 2)];

        for (int i = 0; i < profileData.length; i++){
            profileData[i] = value[i + 2];
        }

        this.profileListener.profileDataReceived(dayOfWeek, profileData);
    }
    ..

As shown in line 12, the management of received data is delegated to the ``profileDataReceived`` function.
It uses two parameters:

* the day of the week, coded according to the static method ``getDayOfTheWeek`` (line 5)
* the ``profileData`` array, which essentially corresponds to the received notification value, *excluding* the two most
  significant bytes (that have already been used)

Basically, it is a matter of analyzing the two functions mentioned above, which allow to examine in depth the aspects
related to the semantics of each byte. The first one, allows us to understand how the days of the week are coded within
the application: ``0`` means *Saturday*, ``1`` means *Sunday* and so on.

.. code-block:: java

    public static DayOfWeek getDayOfWeek(byte b) {
        switch (b) {
            case (byte) 0:
                return DayOfWeek.SATURDAY;
            case (byte) 1:
                return DayOfWeek.SUNDAY;
            case (byte) 2:
                return DayOfWeek.MONDAY;
            case (byte) 3:
                return DayOfWeek.TUESDAY;
            case (byte) 4:
                return DayOfWeek.WEDNESDAY;
            case (byte) 5:
                return DayOfWeek.THURSDAY;
            default:
                return DayOfWeek.FRIDAY;
        }
    }

The second one shows how to interpret all the remaining bytes. :download:`Here <../source_examples/profileDataReceived.java>` is the
original code extracted from the application. We report the main points to understand how it works.

.. code-block:: java

    public void profileDataReceived(DayOfWeek dayOfWeek, byte[] profileData) {

        //creates a list of pairs (temperature, time)
        List<ProfileDataPair> dataPairs = new ArrayList();

        for (i = 0; i < profileData.length; i += 2) {
            //reads two byte at a time and creates (temperature, time) pairs
            int time = (profileData[i + 1] & 255) * 10;
            dataPairs.add(new ProfileDataPair(((double) profileData[i]) / 2.0d, time));
            if (time == 1440) break;
        }

        //find the base temperature to keep outside the programmed ranges
        double baseTemperature = getBaseTemperature(dataPairs);
        ...

        //create a list of Period.
        //each Period contains the data of a range programmed by the user
        List<Period> periods = new ArrayList();

        for (i = 0; i < dataPairs.size(); i++) {

            ProfileDataPair pair = (ProfileDataPair) dataPairs.get(i); //get a Pair

            //if the temperature of the pair is different from the base temperature
            //then the user has entered a schedule for a certain period
            if (pair.temperature != baseTemperature) {

                Period currentPeriod = new Period(); //create a Period

                if(i>0){
                    //the start time of the Period is the end of the previous pair
                    currentPeriod.setStarttimeAsMinutesOfDay(((ProfileDataPair) dataPairs.get(i - 1)).time);
                }
                ...

                //no more than 3 periods can be set
                if (periods.size() < 3)
                    periods.add(currentPeriod);
            }
        }

        ...
        //show the interpreted data in the application
    }

We can therefore deduce that:

* The bytes of the received notification must be interpreted as consecutive pairs ``(temperature, time)``.
  Each pair indicates the temperature to be kept until a certain time.
* In each pair the time is multiplied by 10, while the temperature is divided by 2
* It is possible to identify the base temperature with the algorithm described by the function ``getBaseTemperature``
* The ranges programmed by the user are those that don't use the base temperature and are *at most three*.

This allows us to understand the meaning of the entire notification. An example of the possible values received is
shown in the :ref:`Notifications <notifications_description>` section dedicated to the valve protocol.

Finally, it is interesting to note how the code inside the package ``de.eq3.ble.android.api.command`` and the content
of the files concerning the management of notifications do not involve parameters related to the mode in which
the radiator valve is.

---------------------------

.. [6] `Wikipedia - Class Diagram <https://en.wikipedia.org/wiki/Class_diagram>`_
.. [7] `Wikipedia - Integrated Development Environment <https://en.wikipedia.org/wiki/Integrated_development_environment>`_
.. [8] `ArgoUml <http://argouml.tigris.org/>`_
.. [9] `Umbrello UML Modeller <https://umbrello.kde.org/>`_
.. [10] `BOUML <https://www.bouml.fr/>`_
.. [11] `Modelio <https://www.modelio.org/>`_
