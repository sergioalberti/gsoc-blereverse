
Other Useful Guides
====================

.. _logging_emulator:

Logging With An Emulator
------------------------

As already mentioned and depending on your needs, there are various advantages in using an emulator. Among these:

+ do **not** need a physical device to do the job
+ possibility to easily have a *specific OS* or other features (e.g. root)
+ do not put your *privacy* at risk by installing applications from untrusted sources
+ *sandboxing* (for example through the use of `Firejail <https://firejail.wordpress.com/>`_)

The only "obstacle" that arises in the use of an emulator is due to the Bluetooth communication.
However, it *should* be possible to perform logging and run the application through a **virtual
machine** (like `Virtualbox <https://www.virtualbox.org/>`_) and a **BLE usb dongle**.

`This post <https://chrislarson.me/blog/emulate-android-and-bluetooth-le-hardware/>`_ can be a good
point of reference. At the moment we have not been able to test this solution because the dongle we
have available is not recognized by the virtual machine.

Similar Projects
----------------

Below we present some references concerning the reverse engineering of BLE devices. They are *more focused on the network
traffic* than on the Android application, but they can still be useful because they deal with specific devices.

* `Syska Smartlight Rainbow LED bulb <http://www.instructables.com/id/Reverse-Engineering-Smart-Bluetooth-Low-Energy-Dev/>`_

* `MiPow Playbulb Candle <http://nilhcem.com/iot/reverse-engineering-simple-bluetooth-devices>`_

* `Smart Bulb Colorific! light bulb <https://learn.adafruit.com/reverse-engineering-a-bluetooth-low-energy-light-bulb>`_

* `NO 1 F4 Smart Band <https://medium.com/@arunmag/my-journey-towards-reverse-engineering-a-smart-band-bluetooth-le-re-d1dea00e4de2>`_

* `Fitbit BLE protocol <https://pewpewthespells.com/blog/fitbit_re.html>`_

* `Mikroelektronika Hexiwear <https://dzone.com/articles/tutorial-hexiwear-bluetooth-low-energy-packet-snif>`_

|
|
