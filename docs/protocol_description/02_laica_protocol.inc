.. _laica_ps7200l:

-----------------------
Laica PS7200L Protocol
-----------------------

The `PS7200L <https://www.laica.com/product/smart-electronic-scale-with-body-composition-calculator-ps7002/>`_ is a
BLE scale produced by the italian company `Laica <https://www.laica.com/>`_. In addition to measuring weight, it
allows the calculation of:

* fat percentage
* water percentage
* skeletal muscle mass percentage
* skeletal system weight
* base metabolism
* body mass index

|

.. figure:: ../images/laica_PS7200L.png
    :alt: Laica PS7200L
    :scale: 50%
    :align: center

    Laica PS7200L BLE Scale

To use all the features of the scale, the company provides an application, called *laicabodytouch*, for
`Android <https://play.google.com/store/apps/details?id=com.whb.loease.bodytouch&hl=en_US>`_ and iOS devices.
The application receives the data from the scale, keeps track of it and shows it to the user through simple graphs.

.. (atrent) nota curiosa potrebbe essere quella di elencare quali permessi chiede ;)
.. .. [CHIUSO] (sergioalberti) aggiunta

It is interesting to observe the **permissions** required by the application to perform such a "simple" task.
Some of them are quite *ambiguous* and are not essential to use Bluetooth [4]_. For example, some required
permissions are:

* retrieve running apps
* find/add/remove accounts on the device
* read phone status and identity
* view Wi-Fi connections

.. note::

    At present, the PS7200L protocol has been reverse-engineered only in a small part. However, it has been
    included in this guide because it shows aspects that have not been dealt within other sections.

BLE Communication
-----------------

After analyzing the log files using the methods described in the :ref:`Logging Via Android <logging_via_android>`
section, it becomes clear that the application **does not send commands** to the scale.

The operation principle is based on the fact that when a person is on the scale, this starts broadcasting
*advertising packages*. These packages contain all the information about the device and the person using it.
For this reason, there is not even a pairing procedure between the central device and the BLE device.

Through the advertising packages, the scale provides the application **two essential information**:

1. *the weight* in Kg
2. a value used to derive *all the other parameters* (such as *fat%*, *water%*, etc..)

.. note::

    To achieve the goal of point 2 (derive all the parameters), the application also uses three *manually* entered
    data: gender, age and height.

Advertising Packets Content
----------------------------

The structure of an advertising package is clearly described in the Bluetooth specifications [3]_. In addition to a
field that indicates the MAC address of the device who sent the package, there is an **Advertising Data** field that
basically represents the payload.

Referring to the way *Wireshark* presents the packages (see :ref:`wireshark_log_adv_fig`), the data that compose the
communication protocol between the two devices are the *12 Byte* under ``Advertising Data > Manufacturer Specific > Data``.
Their meaning is as follows::

    ..
    byte (2,3): weight*10 (in Kg)
    byte (4,5): used to derive other parameters (fat%, water%, etc..)
    ..

At the moment it is not clear how the value of the *bytes (4,5)* can lead to the calculation of all the other data. Even
decompiling the Android application the analysis is blocked by the fact that these two bytes are used as input to a
function called ``getHealth()`` in the **proprietary library** ``libyohealth.so``.

We decompiled the library using ``objdump`` (with an
`ARM toolchain <https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads>`_) and the
`RetDec decompiler <https://retdec.com/>`_, which provides output in C language. However, for obvious reasons, the results
are not easily readable. The outputs obtained from the decompilation process are available
`here <https://gitlab.com/sergioalberti/gsoc-blereverse/tree/master/laica_PS7200L_reveng/libyohealth_source>`_.

----------------------------------

.. [3] `Bluetooth Core Specification 5.0, Volume 2, Part E, Page 1193 <https://www.bluetooth.com/specifications/bluetooth-core-specification>`_
.. [4] `Android Bluetooth Permissions <https://developer.android.com/guide/topics/connectivity/bluetooth#Permissions>`_
