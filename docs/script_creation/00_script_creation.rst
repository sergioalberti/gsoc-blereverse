.. _script_creation_sec:

================
Script Creation
================

This section wants to introduce some useful tools to search for Bluetooth devices and to communicate with them.
In particular, we consider the tools provided by the *BlueZ* stack. Then we explain, as an example, some details
on the scripts created for the management of the *Eq3 Eqiva* radiator valves discussed in the
:doc:`Introduction <../introduction/00_introduction>`.

.. include:: 01_bluez_stack.inc

.. include:: 02_eq3eqiva_script.inc

.. include:: 03_laicaPS7200L_script.inc
